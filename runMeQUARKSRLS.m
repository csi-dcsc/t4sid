% QUARKS: Identification of multi-dimensionals autoregressive models with
% sums-of-Kronecker parametrization
%
% Only implemented for I = J
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

% generate the model 
params.I = [10 15];        % output dimensions
params.J = params.I;        % input dimensions
d = length(params.I);      % tensor order
params.r = 2;              % Kronecker rank
s = 2;                    % length of FIR filter
opt.struct = 'random';
M = SOK_fir(params,s,opt); 

% generate input-output data
Nt = 4e3;
Nid = floor(2/3*Nt);
Nval = Nt-Nid;
snr = 20;
[Uid,Yid] = SOK_iofir(params,Nid,snr,M);
[Uval,Yval] = SOK_iofir(params,Nval,snr,M);

% estimate a quarks-rls model
forgetting_factor = 1;
tic
[M_hat,iter,cv] = QUARKS_RLS(params,Uid,Yid,s,forgetting_factor);
time = toc;
figure,semilogy(iter(1:Nid-1,2),'Linewidth',0.5)               
ylabel('Relative RMSE for the output'),xlabel('Time sample'),grid on, drawnow
figure,semilogy(cv(1:Nid-1,1:d)),
ylabel('Frobenius norm between two consecutive updates'),xlabel('Time sample'),grid on,drawnow

% check on validation data
Yhat_val = SOK_ofir(params,M_hat,Uval);
VAF = get_vaf(Yval,Yhat_val);
fprintf('QUARKS-RLS. VAF: %6.2f Time(s): %6.2f \n',VAF,time);



