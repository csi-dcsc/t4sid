The toolbox contains Matlab code for identifying the spatial-temporal dynamics of multi-dimensional systems (both deterministic, or stochastic). Model structures are auto-regressive and state-space. 
We also propose computational tools to solve large scale discrete Lyapunov and algebraic Riccati equations. 

There are seven main files. 
1. Start by running runMeFirst, and all subfolders are added to the path.
2. runMeTutorial.m
This file presents the basic tensor operations and the multi-level matrices that we deal with. It is suggested to run it with Run And Advance.
3. runMeQUARKS.m
This file identifies a tensor auto-regressive model using batch data. The signal generating model is Finite-Impulse-Response in order to visualize the global convergence of the Alternating Least Squares. 
4. runMeQUARKSRLS.m
This file identifies a tensor auto-regressive model recursively. The signal generating model is Finite-Impulse-Response in order to visualize the global convergence of the algorithm.
5. runMeApproxInverse.m
Performing even standard linear algebra operations such as addition, multiplication or inversion on certain type of matrices may destroy its original structure. Structure-preserving properties matter in deriving 
elegant and scalable algorithms. This file determines whether a matrix written as a sum of few Kronecker terms admits a low-Kronecker inverse (or, at least, be approximated by).
6. runMeK4SID.m
This file identifies state-space models when the matrices are written with a Kronecker product of d matrices, d larger or equal than 2.
7. runMeDLyap.m
This file solves efficiently a discrete Lyapunov equation when the matrices are written with a sum of Kronecker products. Under certain conditions, the solution is also a sum of Kronecker products.
8. runMeDRic.m
This file solves efficently a discrete algebraic Riccati equation when the matrices are written with a sum of Kronecker products. Under certain conditions, the solution is also a sum of Kronecker products.

---------------

The Matlab code requires the TensorLab toolbox: 
Vervliet N., Debals O., Sorber L., Van Barel M. and De Lathauwer L. Tensorlab 3.0, Available online, Mar. 2016. URL: https://www.tensorlab.net/

---------------

The toolbox can be cited as:
Sinquin B., Varnai P., Monchen G. and Verhaegen M., "Tensor toolbox for identifying multi-dimensional systems", Version 1.0 available online. URL: https://bitbucket.org/csi-dcsc/t4sid

Suggestions for improvement are always welcome. We would be happy to hear from any user, and how these tools can better fit your application.
Contact: baptiste.sinquin@gmail.com

---------------
References:

Monchen, G., "Recursive Kronecker-based Vector Auto-Regressive identification for large-scale adaptive optics systems", Master thesis, TU Delft, 2017. Online: https://repository.tudelft.nl/islandora/object/uuid%3A530e5665-3568-4063-827a-b2353a70a81a
Monchen, G., Sinquin, B., and Verhaegen, M., "Recursive Kronecker-based Vector Auto-Regressive identification for large-scale Adaptive Optics", Accepted for publication in IEEE Control for Systems Technology, 2018
Sinquin, B., and Verhaegen, M., "QUARKS: Identification of Large-Scale Kronecker Vector-AutoRegressive models", Accepted for publication in IEEE Transactions on Automatic Control, 2018. Preliminary version online: https://arxiv.org/abs/1609.07518
Sinquin, B., and Verhaegen, M., "K4SID: Large-scale subspace identification with Kronecker modeling", Accepted for publication in IEEE Transactions on Automatic Control, 2018. Online: https://ieeexplore.ieee.org/document/8362723/
Sinquin, B., and Verhaegen, M., "Tensor-based predictive control for extremely large-scale adaptive optics", Accepted for publication in JOSA A, 2018
Sinquin, B., "Structured matrices for predictive control for large and multi-dimensional systems, with application to adaptive optics", PhD thesis at Delft Center for Systems and Control, TU Delft, 2019.
Varnai, P., "Exploiting Kronecker structures, with application to optimizations problems arising in the field of adaptive optics", Master thesis, TU Delft, 2017. Online: https://repository.tudelft.nl/islandora/object/uuid%3A98f7cf6e-6ded-4f50-8df7-89944d6a0830

Although some of these papers are not all currently publicly available, they can be provided on request. 
