function xx = get_C_adj(Z,rX,rY,J,x)

xx = rY*Z(1:J(x)^2,1:rX);

end