function obj = evalObj(F,B,C,X,Y)
% evalObj - evalutes ||F - (sum_j kron(Bj,Cj))*(sum_i kron(Xi,Yi))||_F
% in a fast manner
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

obj = 0;
M = length(B);
N = length(X);

% If F is full matrix, there's nothing to do, just calculate it all
if (~iscell(F))
    sumXY = 0;
    for i = 1:N
        sumXY = sumXY + kron(X{i},Y{i});
    end
    sumBC = 0;
    for j = 1:M
    	sumBC = sumBC + kron(B{i},C{i}); 
    end
    obj = norm(F - sumBC*sumXY,'fro');
    return;
end

% Continue optimal calculation if F is also Kronecker
P = size(F,1);

% Precalculate all possible BX, CY terms
BX = cell(M*N,1);
CY = cell(M*N,1);
for j = 1:M
    for i=1:N
        BX{(i-1)*M+j} = B{j}*X{i};
        CY{(i-1)*M+j} = C{j}*Y{i};
    end
end

% Calculate term <F,F>
for i1 = 1:P
    for i2 = i1:P
        if (i1 == i2)
            obj = obj + sum(dot(F{i1,1},F{i2,1}))*sum(dot(F{i1,2},F{i2,2}));
        else
            obj = obj + 2*sum(dot(F{i1,1},F{i2,1}))*sum(dot(F{i1,2},F{i2,2}));
        end
    end
end

% Calculate term <F,AZ>
for i1 = 1:P
    for i2 = 1:M*N
        obj = obj - 2*sum(dot(F{i1,1},BX{i2}))*sum(dot(F{i1,2},CY{i2}));
    end
end

% Calculate term <AZ,AZ>
for i1 = 1:M*N
    for i2 = i1:M*N
        if (i1 == i2)
            obj = obj + sum(dot(BX{i1},BX{i2}))*sum(dot(CY{i1},CY{i2}));
        else
            obj = obj + 2*sum(dot(BX{i1},BX{i2}))*sum(dot(CY{i1},CY{i2}));
        end
    end
end
obj = real(obj);
obj = sqrt(obj);

end
