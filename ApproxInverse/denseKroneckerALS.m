function [X,Y, iterNum, relchange, res] = denseKroneckerALS(F, B, C, X0, Y0, N, opts)
% denseKroneckerALS
% Solves min(X,Y) ||F - (sum_i^M (B_i kron C_i)) * (sum_j^N(X_j kron Y_j))||_F
% Assume X, Y are each full dense square matrices
%
% ! Tested only with full matrices so far. Should be adaptable to
% rectangular case
%
% INPUTS:
% F         - n^2 by n^2 matrix of the optimization problem or P by 2 cell array of Kronecker pairs.
% B,C       - n by n matrices of the problem, provided as cell arrays
% X0,Y0     - optional initial n by n matrices for the unknowns
% opts      - structure of algorithm options:
%   maxIter   - maximum ALS iterations
%   relSol    - termination tolerance for relative change in variables
%   relObj    - termination tolerance for relative change in objective
%   useFast   - flag whether or not to employ Nesterov-type acceleration
%   normalize - whether or not to normalize Kronecker pairs in iterations
%   showInfo  - flag to display convergence information
%
%OUTPUTS:
% X,Y       - cell array of n by n matrices describing the solution
% iterNum   - actual number of ALS iterations required for given tolerance
% relchange - relative change of solution during ALS iterations 
% res       - achieved residual error
%
% Note that the full Kronecker matrices are only formed and calculated with
% if either opts.relObj is present or 'res' is specified as an output.
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

%% Parse input
% Convert input to cells for easier handling
if (~isempty(X0) && ~iscell(X0))   
   X0 = num2cell(X0,[1 2]);
end
if (~isempty(Y0) && ~iscell(Y0))
   Y0 = num2cell(Y0,[1 2]);
end
if (~iscell(B))   
   B = num2cell(B,[1 2]);
   C = num2cell(C,[1 2]);
end
M = length(B);      % number of sum terms B_i, C_i
n = size(B{1},1);
m = size(C{1},1);
P = size(F,1); 
% Check if input is composed of size n matrices
% if (iscell(F))
%    P = size(F,1); 
%    for i=1:P
%        if (size(F{i,1},1) ~= n || size(F{i,1},2) ~= n || size(F{i,2},1) ~= n || size(F{i,2},2) ~= n)
%            fprintf('Error: matrix F should be of consistent dimension\n');
%        end
%     end
% else
%     if (size(F,1) ~= n^2 || size(F,2) ~= n^2)
%         fprintf('Error: matrix F should be of consistent dimension\n');
%     end
% end
% for i=1:M
%     if (size(B{i},1) ~= n || size(B{i},2) ~= n ||...
%             size(C{i},1) ~= n || size(C{i},2) ~= n)
%         fprintf('Error: Matrices Bi, Ci should have the same size\n');
%         break;        
%     end
% end

% Default maximum iterations & relative tolerance
if (~isfield(opts,'relSol'))
    opts.relTol = 1e-6;
end
if (~isfield(opts,'maxIter'))
    opts.maxIter = 100;
end
if (~isfield(opts,'maxADMMIter'))
    opts.maxADMMIter = 500;
end
if (~isfield(opts,'useFast'))
    opts.useFast = false;
end
if (~isfield(opts,'relObj'))
    opts.relObj = [];
end
if (~isfield(opts,'normalize'))
    opts.normalize = false;
end
if (~isfield(opts,'showInfo'))
    opts.showInfo = false;
end
if (~isfield(opts,'fastRestart'))
    opts.fastRestart = false;
end
if (~isfield(opts,'symmetric'))
	opts.symmetric = false;
end

% Check if there will be a need to evaluate objective value
if (nargout == 5 || ~isempty(opts.relObj))
    normFlag = true;
else
    normFlag = false;
end

%% Initialize ALS
% Initialize solution (X and Y are cell arrays for each N unknown pair!)
X = cell(N,1);
Y = cell(N,1);
for i = 1:N
    if (isempty(X0))
        X{i} = randn(n);
    else
        X{i} = X0{i};
    end
    if (isempty(Y0))
        Y{i} = randn(m);
    else
        Y{i} = Y0{i};
    end
    
    % Start as symmetric by default
    if (opts.symmetric)
       X{i} = (X{i} + X{i}')/2; 
       Y{i} = (Y{i} + Y{i}')/2; 
    end
    
    % Normalize initial matrices if necessary
    if (opts.normalize)
        ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
        X{i} = X{i}/ratio;
        Y{i} = Y{i}*ratio;
    end
end

% options for solving triangular equations fast
linopts.UT = true;

% compute Kronecker rearrangement of matrix F
if (iscell(F))
    f = cell(size(F));
    for i = 1:P
       f{i,1} = vec(F{i,1});
       f{i,2} = vec(F{i,2});
    end
else
    RF = rearrangeKronecker(F,n,n,m,m);
end

% precalculate some matrices and their needed factorizations
BMAT = zeros(n, n*M);
CMAT = zeros(m, m*N*M);
for j = 1:M
   BMAT(:,(j-1)*n+1:j*n) = B{j};
   CMAT(:,(j-1)*m+1:j*m) = C{j};
end
[QBMAT,RBMAT] = qr(BMAT,0);
[QCMAT,RCMAT] = qr(CMAT,0);

  
%% ALS iterations
iter = 0;
loopCondition = true;
res = zeros(opts.maxIter,1);
relchange = zeros(opts.maxIter,1);
fk = cell(n,1);
nu = 1;
alpha = 1;
normId = n;
newNorm = 1e8;
Ytilde = Y;             % accomodates acceleration
while(loopCondition)
    iter = iter + 1;
    if (opts.showInfo)
       fprintf('Iteration %d:\n',iter); 
    end
    
    % Save previous iteration's solution to evaluate change later
    OX = X;
    OY = Y;
    
    %% Solve for each column of every X_i at the same time
    % Assemble 'C' matrix which is common for all X_i's + factorize
    yC = zeros(M*N, m*m);
    for k = 1:m
        for i = 1:M
            for j = 1:N
                yC((j-1)*M+i,(k-1)*m+1:k*m) = (C{i}*Ytilde{j}(:,k))';
            end
        end
    end
    [QyC,LyC] = qr(yC',0); LyC = LyC';      % QC not transposed, as it's used as it is computed here
    H = computeH(M,N,n,RBMAT, LyC);
	for i = 1:n
		if (iscell(F))
			fk{i} = 0;
			for j=1:P
				fk{i} = fk{i} + vec((QBMAT'*f{j,1}((i-1)*n+1:i*n))*(f{j,2}'*QyC));
            end
        else
            fk{i} = vec(QBMAT'*(RF((i-1)*m+1:i*m,:)*QyC));
        end
	end
	if (opts.symmetric)
		X = symmetricKroneckerALS_ADMM(H,fk,X,opts,n);
	else
		[QH,RH] = qr(H,0);
		for i = 1:n
			fk{i} = QH'*fk{i};
			x = linsolve(RH,fk{i},linopts);
			for k = 1:N
				X{k}(:,i) = x((k-1)*n+1:k*n);            
			end
		end
	end
    
    %% Solve for each column of every Y_i at the same time
    % Assembe 'B' matrix which is common for all Y_i's
    xB = zeros(M*N, n^2);
    for k = 1:n
        for i = 1:M
            for j = 1:N
                xB((j-1)*M+i,(k-1)*n+1:k*n) = (B{i}*X{j}(:,k))';
            end
        end
    end     
    [QxB,LxB] = qr(xB',0); 
    LxB = LxB';      % QC not transposed, as it's used as it is computed here
    H = computeH(M,N,m,RCMAT, LxB);
	for i = 1:m
		if (iscell(F))
			fk{i} = 0;
			for j=1:P
				fk{i} = fk{i} + vec((QCMAT'*f{j,2}((i-1)*m+1:i*m))*(f{j,1}'*QxB));
            end
        else
            fk{i} = vec(QCMAT'*(RF(:,(i-1)*n+1:i*n)'*QxB));
        end
    end
	if (opts.symmetric)
		Y = symmetricKroneckerALS_ADMM(H,fk,X,opts,n/1000);	
	else
		[QH,RH] = qr(H,0);
		for i = 1:m
			fk{i} = QH'*fk{i};
			y = linsolve(RH,fk{i},linopts);
			for k = 1:N
				Y{k}(:,i) = y((k-1)*m+1:k*m);            
			end
		end
    end    
    
    % Calculate solution as a sum of X_i kron Y_i
    if (normFlag)
        % display objective value  
        oldNorm = newNorm;
        newNorm = evalObj(F,B,C,X,Y)/normId;
        relObj = abs(newNorm - oldNorm)/newNorm;
        res(iter) = newNorm;
        if (opts.showInfo)
            fprintf('\tobjective value: %f\n',newNorm);
        end
        if iter > 1
            if res(iter) > res(iter-1)*nu;
               alpha = 1;
               iter = iter - 1;
            end
        end
    end
    
%     if (opts.useFast)
%         oalpha = alpha;
%         alpha = (1 + sqrt(4*alpha^2+1))/2;
%         if (opts.fastRestart > 0)
%             if (mod(iter,opts.fastRestart) == 0)
%                 oalpha = 1;
%                 alpha = 1;
%             end
%         end
%         for i = 1:N
%             Ytilde{i} = Y{i} + (oalpha-1)*(Y{i}-OY{i})/alpha;
%         end
%     else
        for i = 1:N
            Ytilde{i} = Y{i};
        end
%     end
    
%     % Normalize each Kronecker pair
%     if (opts.normalize)
%         for i = 1:N
%            ratio = sqrt(norm(X{i},'fro')/norm(Y{i},'fro'));
%            X{i} = X{i}/ratio;
%            Y{i} = Y{i}*ratio;
%            Ytilde{i} = Ytilde{i}*ratio;
%         end
%     end    
    
    % Evaluate relative change in solution
    relSol = 0;
    for i = 1:N
        relSol = max(relSol,  norm(X{i} - OX{i},'fro')/ norm(X{i},'fro'));
        relSol = max(relSol,  norm(Y{i} - OY{i},'fro')/ norm(Y{i},'fro'));
    end   
    relchange(iter) = relSol;
    if (opts.showInfo)
        fprintf('\trelative change: %f\n',relSol);
    end

    loopCondition = (relSol > opts.relSol) && (iter < opts.maxIter);
    if (~isempty(opts.relObj))
        loopCondition = loopCondition && (relObj > opts.relObj);
    end
end

iterNum = iter;
relchange = relchange(1:iterNum);
if (nargout == 5)
    res = res(1:iterNum);
end


end

