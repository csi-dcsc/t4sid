function xx = call_C_adj(rX,rY,J,x,Z)
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

nx = J(x)^2*rX;
xx = zeros(nx,1);
for j = 1:rY
    xx = xx+vec(Z((j-1)*J(x)^2+1:j*J(x)^2,(j-1)*rX+1:j*rX));
end

end
