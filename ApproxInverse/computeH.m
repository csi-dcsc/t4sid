function Htilde = computeH(M,N,n,R,L)
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

% initialize H matrix
H = zeros(n, M*N^2*n);

% Cycle through each block-row of R, by summand number and unknown number
% Cycle through each column of L
for j = 1:M*N           
    % Cycle through all unknowns by M and N
    for kM = 1:M
       for kN = 1:N
          % Assemble coefficient for unknown N belonging to this
          % column of L
          H(:,(j-1)*N*n+((kN-1)*n+1:kN*n)) = H(:,(j-1)*N*n+((kN-1)*n+1:kN*n)) +...
              R(:,(kM-1)*n+1:kM*n) * L((kN-1)*M+kM,j);                   
       end
    end           
end

% rearrange H for least squares solution of unknowns
Htilde = zeros(M*N*n,N*n);
for i = 1:M*N
    Htilde((i-1)*n+1:i*n,:) = H(:, (i-1)*N*n+1:i*N*n);
end

end

