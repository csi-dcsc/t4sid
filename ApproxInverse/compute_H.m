function H = compute_H(J,n,x,ra,r,C,Y)
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

H = zeros(J(x)*n,J(x)*ra);
for i = 1:ra
    temp = zeros(J(x)*n,J(x));
    for j = 1:r
        temp = temp+kron(C((i-1)*r+j,:)',Y{x,j});
    end
    H(:,(i-1)*J(x)+1:i*J(x)) = temp;
end 

end
