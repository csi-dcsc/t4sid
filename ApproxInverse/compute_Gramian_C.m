function P = compute_Gramian_C(Ybar,Fbar,rX,rY,J,x)
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

Y = Ybar'*Ybar;
F = Fbar*Fbar';
P = zeros(rX*J(x));
id = 1;
for jj = 1:rX
    for ii = 1:J(x):J(x)^2
       mat = Y(1:J(x):rY*J(x)^2,[ii:J(x)^2:rY*J(x).^2])*F([jj:rX:rX*rY],:);
       temp = zeros(rX*J(x),1);
       for j = 1:rY 
           temp = temp+vec(mat((j-1)*J(x)+1:j*J(x),(j-1)*rX+1:j*rX));
       end
       P(:,id) = temp;
       id = id+1;
    end
end
 
end