function [X, Y, relchange, res] = denseKroneckerALS_progressive(F, B, C, X0, Y0, N, opts)
% denseKroneckerALS_progressive
% Solves min(X,Y) ||F - (sum_i^M (B_i kron C_i)) * (sum_j^N(X_j kron Y_j))||_F
% in an approximate manner, building it up one term at a time
% 
% Peter Varnai, August 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

%% Parse input
% Convert input to cells for easier handling
if (~iscell(B))   
   B = num2cell(B,[1 2]);
   C = num2cell(C,[1 2]);
end
M = length(B);      % number of sum terms B_i, C_i
P = size(F,1);      % number of sum terms in objective

% Check if input is composed of size n matrices
n = size(B{1},1);
for i=1:P
    if (size(F{i,1},1) ~= n || size(F{i,1},2) ~= n || size(F{i,2},1) ~= n || size(F{i,2},2) ~= n)
        fprintf('Error: matrix F should be of consistent dimension\n');
    end
end
for i=1:M
    if (size(B{i},1) ~= n || size(B{i},2) ~= n ||...
            size(C{i},1) ~= n || size(C{i},2) ~= n)
        fprintf('Error: Matrices Bi, Ci should have the same size\n');
        break;        
    end
end

% Default maximum iterations & relative tolerance
if (~isfield(opts,'relSol'))
    opts.relTol = 1e-6;
end
if (~isfield(opts,'maxIter'))
    opts.maxIter = 50;
end
if (~isfield(opts,'useFast'))
    opts.useFast = false;
end
if (~isfield(opts,'relObj'))
    opts.relObj = [];
end
if (~isfield(opts,'normalize'))
    opts.normalize = true;
end
if (~isfield(opts,'showInfo'))
    opts.showInfo = false;
end
if (~isfield(opts,'symmetric'))
    opts.symmetric = false;
end
if (~isfield(opts,'solver'))
    opts.solver = @denseKroneckerALS_gradient;
end
if (~isfield(opts,'update'))
    opts.update = 'none';
end
if (~isfield(opts,'loop'))
    opts.loop = false;
end
if (~isfield(opts,'maxLoopCount'))
    opts.maxLoopCount = 500;
end

ATFX = cell(M*P,1);
ATFY = cell(M*P,1);
for i = 1:M
    for j = 1:P
       ATFX{(j-1)*M+i} = B{i}' * F{j,1};
       ATFY{(j-1)*M+i} = C{i}' * F{j,2};
    end    
end

ATAX = cell(M^2,1);
ATAY = cell(M^2,1);
for i = 1:M
    for j = 1:M
       ATAX{(j-1)*M+i} = B{i}' * B{j};
       ATAY{(j-1)*M+i} = C{i}' * C{j};
    end    
end

% Initialize previous approximation
X = cell(N,1);
Y = cell(N,1);
xvec = zeros(n^2,N);
yvec = zeros(n^2,N);
res = [];
relchange = [];
loopCount = 0;
i = 0;
while(i<N)
    i = i + 1;
    % Initialize solution of this step
    if loopCount == 0
        if (isempty(X0))
            X{i} = randn(n);
        else
            X{i} = X0{i};
        end
        if (isempty(Y0))
            Y{i} = randn(n);
        else
            Y{i} = Y0{i};
        end
    end
    
    
    % Solve LS problem for this X, Y pair
    if (nargout == 4)
        [solX,solY,~,solchange, solres] = opts.solver(F, B, C, X{i}, Y{i}, 1, opts);        
        res = [res; solres];
    else
        [solX,solY,~,solchange] = opts.solver(F, B, C, X{i}, Y{i}, 1, opts);
    end
    relchange = [relchange; solchange];
    X{i} = solX{1};
    Y{i} = solY{1};
    
      
    % Calculate better approximation within given subspace (could be done
    % faster)
    if (strcmp(opts.update, 'subspace'))
        xvec(:,i) = vec(X{i});
        yvec(:,i) = vec(Y{i});
        if (i > 1)
            [QX,~] = qr(xvec(:,1:i),0); 
            [QY,~] = qr(yvec(:,1:i),0);

            % Assemble right hand side
            bvec = zeros(i^2,1);
            for j = 1:i % column 'X'
                for k = 1:i % row 'Y'
                    for l = 1:length(ATFX)
                        bvec((k-1)*i+j) = bvec((k-1)*i+j) + sum(dot(ATFX{l},mat(QX(:,j))))*sum(dot(ATFY{l},mat(QY(:,k))));
                    end
                end
            end
            % Assemble matrix (can be greatly simplified)
            HMAT = zeros(i^2);
            for j = 1:i
                for k = 1:i
                    for jj = 1:i
                        for kk = 1:i
                            for l = 1:length(ATAX)
                                val = sum(dot(ATAX{l}*mat(QX(:,jj)),mat(QX(:,j))))*sum(dot(ATAY{l}*mat(QY(:,kk)),mat(QY(:,k))));
                                HMAT((k-1)*i+j, (kk-1)*i + jj) = HMAT((k-1)*i+j, (kk-1)*i + jj) + val;                        
                            end
                        end
                    end                
                end
            end
            sol = HMAT\bvec;
            for j = 1:i
                X{j} = mat(QX(:,j));
                Y{j} = 0;
                for k = 1:i
                    Y{j} = Y{j} + sol((k-1)*i+j)*mat(QY(:,k));
                end
            end
        end
    elseif (strcmp(opts.update, 'complete'))
        if (nargout == 4)
            [solX,solY,~,solchange,solres] = opts.solver(F(1:P,1:2), B, C, X(1:i), Y(1:i), i, opts);
            res = [res; solres];
            X(1:i) = solX;
            Y(1:i) = solY;
        else
            [solX,solY,~,solchange] = opts.solver(F, B, C, X(1:i), Y(1:i), i, opts);
            X(1:i) = solX;
            Y(1:i) = solY;
        end
        relchange = [relchange; solchange];
    end
    
    if (opts.normalize)
        for j = 1:i
           ratio = sqrt(norm(X{j},'fro')/norm(Y{j},'fro'));
           X{j} = X{j}/ratio;
           Y{j} = Y{j}*ratio;
           xvec(:,j) = vec(X{j});
           yvec(:,j) = vec(Y{j});
        end
    end


    % Calculate solution as a sum of X_i kron Y_i
    if (nargout == 4)  
        % display objective value  
        newNorm = evalObj(F(1:P,:),B,C,X(1:i),Y(1:i));        
        res = [res; newNorm];
        if (opts.showInfo)
            fprintf('\tprojected obj value: %f\n',newNorm);
        end
    end
    
    % Include new Kronecker terms in cell array F
    for ii = 1:i
        for j = 1:M
           F{P+(ii-1)*M+j,1} = -B{j}*X{ii};
           F{P+(ii-1)*M+j,2} = C{j}*Y{ii};
        end
    end
    
    if (i == N && opts.loop == true)
       if (loopCount == opts.maxLoopCount)
           break;
       end
       loopCount = loopCount + 1;
       i = i-1;
       F = F([1:P P+M+1:end],:);
       X(1:N) = X([2:N 1]);
       Y(1:N) = Y([2:N 1]);       
       if (strcmp(opts.update, 'subspace'))
            xvec(:,1:N-1) = xvec(:,2:N);
            yvec(:,1:N-1) = yvec(:,2:N);
       end
    end
end



end

