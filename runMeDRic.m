% Solve the Riccati equation written in discrete time 
% using either Newton's method or the sign iteration
%
% Assumes the factors are SSS.
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

clc; clear; close all

% generate the matrices 
params = [];
params.r = 1;                 
params.I = [10 10];     
params.J = params.I;            
N = prod(params.I);
params.J = params.I;           
params.n = params.J;           
d = length(params.I);    
% Important note: the Kronecker rank may be increased in which case it is
% necessary to play with the values of threshold or spectral radius of each
% factor matrix to form a full matrix which is stable, without computing
% many eigenvalue decompositions of the latter especially when the sizes
% are large.       
opt = [];
sys = SOK_ss(params,opt); 
sigma = 0.5;
params.r = 2;
Q = SOK_spdmat(params,sigma);
sigma_r = 1;
R = cell(d,1);
for x = 1:d
    R{x} = sigma_r^(1/d)*eye(params.J(x));
end

% form the unstructured matrices
Ag = SOK_getmat(sys.A)'; 
Bg = SOK_getmat(sys.C)';
Qg = SOK_getmat(Q);
Rg = SOK_getmat(R);

% using the dare function - Matlab
tic
[~,~,K_m] = dare(Ag,Bg,Qg,Rg);
time_matlab = toc;

% using Newton's method solving a sequence of Lyapunov equations
tic
K_newton = solveDRic_viaNewton(Ag,Bg,Qg,Rg);
time_newton_unstructured = toc;

tic
K_newton = solveDRic_viaNewtonHewer(Ag,Bg,Qg,Rg);
time_newtonhewer_unstructured = toc;

% with Kronecker-structure
ra = 2; 
A = SOK_transpose(sys.A);
B = SOK_transpose(sys.C);
% - using Newton's method
tic
[K,~] = SOK_dRicNewton(A,B,Q,R,ra);
time_kron = toc;
fprintf('Newton with SOK: Residual RMSE (in log10) %6.2f, Ratio of computation times: %6.2f \n',log10(rmse(SOK_getmat(K)',K_m)),time_kron/time_newton_unstructured);
% - using Newton-Hewer's method
tic
K = SOK_dRicNewtonHewer(A,B,Q,R,ra);
time_kron = toc;
fprintf('Newton-Hewer with SOK: Residual RMSE (in log10) %6.2f, Ratio of computation times: %6.2f\n',log10(rmse(SOK_getmat(K)',K_m)),time_kron/time_newtonhewer_unstructured);
