/* ALS_magma.cu
 *
 * Source file for the ALS solver library using the MAGMA library.
 * Note: You need to have the MAGMA library installed to be able to use this.
 *
 * G. Monchen (2017)
 */

#include <ALS_magma.cuh>
#include <stdio.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
#include "helperFunctions.cuh"

//#define CUSOLVER

/**
 * Constructor of ALS class.
 *
 * @param pRows			Number of rows of Y(k)
 * @param pColumns		Number of columns of Y(k)
 * @param pIterations	Total number of iterations
 * @param pOrderARX		The number of past inputs to consider (ARX model order)
 * @param pALSIter		The number iterations the ALS algorithm should execute
 * @param pRankKron		The kronecker rank of the the model, this is the same for all orders
 */
ALS::ALS(int pRows, int pColumns, int pIterations, int pOrderARX, int pALSIter, int pRankKron) {
	// These variables define the dimensions of the matrices, they need to be defined manually because
	// there is not yet a connection betwen Python and CUDA
	cRows = pRows;
	cColumns = pColumns;
	cIterations = pIterations; 
	cOrderARX = pOrderARX;
	cALSIter = pALSIter;
	cRankKron = pRankKron;
	
	// The number of iterations we can use for identification, this depends on the order of the ARX model
	nIterations = cIterations - cOrderARX + 1;
	
	cudacall(cudaMalloc(&d_data, cRows * cColumns * nIterations * cOrderARX * cRankKron * sizeof(float)));
	
	magma_init();
	magma_int_t m = max(cRows * nIterations, cColumns * nIterations);
	magma_int_t n = max(cColumns, cRows);
	magma_int_t nrhs = n;
	
	ldda = ((m+31)/32)*32;
	lddb = ldda;
	magma_int_t nb = magma_get_sgeqrf_nb(m,n);
	lworkgpu = (m-n + nb)*(nrhs + 2*nb);
	
	magma_smalloc_cpu(&d_buffer, lworkgpu);
	
	printf("\nSolving a system of dimension %d x %d, with %d bytes of memory.\n", m,n,lworkgpu*4);

	//h_sData = (float *) malloc(cRows * cColumns * cIterations * sizeof(float));
	// Matrix containing the correctly formatted data for y(k) to solve for either B or C
	//h_leftMatLS = (float *) malloc(cRows * cColumns * nIterations * sizeof(float));
	//h_rightMatLS = (float *) malloc(cRows * cColumns * nIterations * sizeof(float));
	// The left parameter matrices, AKA C
	h_leftMat = (float *) malloc(cRows * cRows * cOrderARX * cRankKron * sizeof(float));
	// The right parameter matrices, AKA B
	h_rightMat = (float *) malloc(cColumns * cColumns * cOrderARX * cRankKron * sizeof(float));
	// Allocate the data for the GPU, these are the same variables as declared earlier but now stored on the device
	cudacall(cudaMalloc(&d_sData, cRows * cColumns * cIterations * sizeof(float)));
	cudacall(cudaMalloc(&d_uData, cRows * cColumns * cIterations * sizeof(float)));
	cudacall(cudaMalloc(&d_leftMatLS, cRows * cColumns * nIterations * sizeof(float)));
	cudacall(cudaMalloc(&d_rightMatLS, cRows * cColumns * nIterations * sizeof(float)));
	cudacall(cudaMalloc(&d_leftMat, cRows * cRows * cOrderARX * cRankKron * sizeof(float)));
	cudacall(cudaMalloc(&d_rightMat, cColumns * cColumns * cOrderARX * cRankKron * sizeof(float)));
	
	cudacall(cudaMalloc(&d_dataTemp, cRows * cColumns * nIterations * cOrderARX * cRankKron * sizeof(float)));
	
	cudacall(cudaMallocHost(&h_ptrMatB, nIterations * cRankKron * cOrderARX * sizeof(*h_ptrMatB)));
	cudacall(cudaMallocHost(&h_ptrMatC, nIterations * cRankKron * cOrderARX * sizeof(*h_ptrMatC)));
	cudacall(cudaMallocHost(&h_ptrSdata, nIterations * cRankKron * cOrderARX * sizeof(*h_ptrSdata)));
	cudacall(cudaMallocHost(&h_ptrDataTemp, nIterations * cRankKron * cOrderARX * sizeof(*h_ptrDataTemp)));
	
	cudacall(cudaMallocHost((void **) &h_ptrData, sizeof(*h_ptrData)));
	
	//cudacall(cudaMalloc(&d_buffer, cRows * cRows * cOrderARX * cRankKron * sizeof(float)));
	
	// Create a handle for CUBLAS
	cublasCreate_v2(&cublasHandle);
}

/**
 * Destructor of ALS class.
 *
 */
ALS::~ALS() {
	// Free the GPU memory
	cudacall(cudaFree(d_sData));
	cudacall(cudaFree(d_uData));
	cudacall(cudaFree(d_leftMatLS));
	cudacall(cudaFree(d_rightMatLS));
	cudacall(cudaFree(d_leftMat));
	cudacall(cudaFree(d_rightMat));
	cudacall(cudaFree(d_data));
	cudacall(cudaFree(d_dataTemp));
	
	cudacall(cudaFree(d_ptrMatB));
	cudacall(cudaFree(d_ptrMatC));
	cudacall(cudaFree(d_ptrSdata));
	cudacall(cudaFree(d_ptrDataTemp));
	cudacall(cudaFreeHost(h_ptrMatB));
	cudacall(cudaFreeHost(h_ptrMatC));
	cudacall(cudaFreeHost(h_ptrSdata));
	cudacall(cudaFreeHost(h_ptrDataTemp));	
	
	cudacall(cudaFree(d_ptrData));
	cudacall(cudaFreeHost(h_ptrData));

	free(d_buffer);
	
	// Free the allocated memory
	free(h_leftMat);
	free(h_rightMat);
	
	magma_finalize();
	
	cublasDestroy(cublasHandle);
	
	cudaDeviceReset();
}

/**
 * Loads necessary data from text files.
 *
 * When considering the LS problem:
 * 	[ Y(1) ]	[ C Y(0) ] 
 *	[ Y{2) ] =  [ C Y(1) ] B
 *	[  :   ] 	[	:	 ] 
 * 	[ Y(k) ]    [C Y(k-1)] 
 * The data for calculating the matrix multiplication C Y(k-1) is pulled from sData, the full
 * left matrix containing Y(k) should be stored in rightMatLSLoc. For estimating C, the left matrix
 * will contain all Y(k) transposed and should be stored in leftMatLSLoc. Important to note is that
 * C will read the text files in row-major order, but CUDA considers column-major order. This means
 * that it is best to reshape all these matrices to vectors and then save them to file. This is to make
 * sure that the data is correct.
 * @param sDataLoc		Location of the text file containing Y(k) in vectorized form with the total number of 
 *						iterations equal to cIterations
 * @param leftMatLSLoc	Location of the text file containing Y(k)' structured for the LS problem with number of 
 *						iterations equal to cIterations - cARXOrder.
 * @param rightMatLSLoc	Location of the text file containing Y(k) structured for the LS problem with number of 
 *						iterations equal to cIterations - cARXOrder.
 */
void ALS::loadData(const char *sDataLoc, const char *uDataLoc, const char *leftMatLSLoc, const char *rightMatLSLoc) {
	// Allocate memory for these matrices
	h_sData = (float *) malloc(cRows * cColumns * cIterations * sizeof(float));
	h_uData = (float *) malloc(cRows * cColumns * cIterations * sizeof(float));
	h_leftMatLS = (float *) malloc(cRows * cColumns * nIterations * sizeof(float));
	h_rightMatLS = (float *) malloc(cRows * cColumns * nIterations * sizeof(float));

	// Load the files into memory, they are read in row-major order.
	loadMatrixFile(h_sData, sDataLoc);
	loadMatrixFile(h_uData, uDataLoc);
	loadMatrixFile(h_leftMatLS, leftMatLSLoc);
	loadMatrixFile(h_rightMatLS, rightMatLSLoc);
}

/**
 * Loads the neccesary data from a pointer to already assigned memory.
 *
 * @see loadData()
 * @param sDataLoc		Pointer to memory containing Y(k) in vectorized form with the total number of 
 *						iterations equal to cIterations
 * @param leftMatLSLoc	Pointer to memory containing Y(k)' structured for the LS problem with number of 
 *						iterations equal to cIterations - cARXOrder.
 * @param rightMatLSLoc	Pointer to memory containing Y(k) structured for the LS problem with number of 
 *						iterations equal to cIterations - cARXOrder.
 */
void ALS::loadData(float *sData, float *uData, float *leftMatLS, float *rightMatLS) {
	// Just assign the pointer, we will copy the data to the device anyway.
	h_sData = sData;
	h_uData = uData;
	h_leftMatLS = leftMatLS;
	h_rightMatLS = rightMatLS;
}

/**
 * Saves the results of the ALS algorithm to the specified files.
 *
 * @param leftMatLoc	Location of the text file that will contain the left matrix (C)
 * @param rightMatLoc	Location of the text file that will contain the right matrix (B)
 */
void ALS::saveData(const char *leftMatLoc, const char *rightMatLoc) {
	writeMatrixFile(h_leftMat, cRows * cOrderARX * cRankKron, cRows, leftMatLoc);
	writeMatrixFile(h_rightMat, cColumns * cOrderARX * cRankKron, cColumns, rightMatLoc);
	
	// Print the result
	//printMatrix(h_leftMat, cRows, cRows, 0);
	//printMatrix(h_rightMat, cColumns, cColumns, 0);
	
	// Free the memory occupied by the matrices used for the text files
	free(h_sData);
	free(h_uData);
	free(h_leftMatLS);
	free(h_rightMatLS);
}

/**
 * Saves the results of the ALS algorithm to the specified files.
 *
 * @param leftMat	Pointer to allocated memory that will contain the left matrix (C)
 * @param rightMat	Pointer to allocated memory that will contain the right matrix (B)
 */
void ALS::saveData(float *leftMat, float *rightMat) {
	cudacall(cudaMemcpy(leftMat, h_leftMat, cRows * cRows * cRankKron * cOrderARX * sizeof(float), cudaMemcpyHostToHost));
	cudacall(cudaMemcpy(rightMat, h_rightMat, cColumns * cColumns * cRankKron * cOrderARX * sizeof(float), cudaMemcpyHostToHost));
}

/**
 * Runs the ALS algorithm.
 *
 * Make sure that after object creation, either one of the two overloaded functions loadData() is called before running
 * this function. 
 */
void ALS::runALS() {
	// Copy the needed data to device memory
	cudacall(cudaMemcpy(d_sData, h_sData, cRows * cColumns * cIterations * sizeof(float), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_uData, h_uData, cRows * cColumns * cIterations * sizeof(float), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_leftMatLS, h_leftMatLS, cRows * cColumns * nIterations * sizeof(float), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_rightMatLS, h_rightMatLS, cRows * cColumns * nIterations * sizeof(float), cudaMemcpyHostToDevice));
	
	cudacall(cudaMalloc(&d_ptrSdata, nIterations * cRankKron * cOrderARX * sizeof(*d_ptrSdata)));
	cudacall(cudaMalloc(&d_ptrDataTemp, nIterations * cRankKron * cOrderARX * sizeof(*d_ptrDataTemp)));
	cudacall(cudaMalloc(&d_ptrMatB, nIterations * cRankKron * cOrderARX * sizeof(float *)));
	cudacall(cudaMalloc(&d_ptrMatC, nIterations * cRankKron * cOrderARX * sizeof(float *)));

	// Fill the right matrices with random data
	GPUFillRand(d_rightMat, cColumns * cColumns * cOrderARX * cRankKron);
	
	timespec t0, t1;
	
	// Construct multiplication matrices
	// For each ARX order
	for(int i = 0, z2 = 0, z = 0; i < cOrderARX; i++) {
		// For each Kronecker rank
		for(int j = 0; j < cRankKron; j++) {
			// For each iteration
			for(int k = cOrderARX - 1; k < cIterations; k++) {
				// Construct the matrix containing Y(k - 1 + cOrderARX) * B_{i,j}
				// We only create an array with pointers here, which will later on be used as
				// input to the gemmBatched routine of CUBLAS.
				h_ptrMatB[z] = &d_rightMat[z2 * cColumns * cColumns];
				h_ptrMatC[z] = &d_leftMat[z2 * cRows * cRows];
				h_ptrSdata[z] = &d_uData[(k - i) * cRows * cColumns];
				h_ptrDataTemp[z] = &d_dataTemp[z * cRows * cColumns];

				z++;
			}
			
			z2++;
		}
	}
	
	// These calculations are done for each Kronecker rank and ARX order seperately, this is to circumvent the
	// need to reshape the matrix afterwards. We calculate the transpose matrix (which is a wide matrix, with
	// all iterations stacked next to each other, so a matrix of size (cRows x cColumns * nIterations)). This
	// matrix is then transposed to a tall matrix, and finally for each Kronecker rank and ARX order this
	// calculated matrix is stacked next to the previous one.
	cudacall(cudaMemcpy(d_ptrMatB, h_ptrMatB, nIterations * cRankKron * cOrderARX * sizeof(float *), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_ptrDataTemp, h_ptrDataTemp, nIterations * cRankKron * cOrderARX  * sizeof(*d_ptrDataTemp), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_ptrMatC, h_ptrMatC, nIterations * cRankKron * cOrderARX * sizeof(float *), cudaMemcpyHostToDevice));
	cudacall(cudaMemcpy(d_ptrSdata, h_ptrSdata, nIterations * cRankKron * cOrderARX * sizeof(*d_ptrSdata), cudaMemcpyHostToDevice));
	
	struct timeval ti1, ti2;
	double runtime;
	h_ptrData[0] = d_data;
	
	// Copy the array of pointers to the device
	cudacall(cudaMalloc(&d_ptrData, sizeof(*d_ptrData)));
	cudacall(cudaMemcpy(d_ptrData, h_ptrData, sizeof(*d_ptrData), cudaMemcpyHostToDevice));
	
	clock_gettime(CLOCK_MONOTONIC, &t0);
	gettimeofday(&ti1, NULL);
	 
	// The ALS loop
	for(int i = 0; i < cALSIter; i++) {
		//
		// UPDATING THE LEFT MATRICES
		//
	
		// Solve the equations for updating the left matrix
		
		updateLeft();
		
		//
		// UPDATING THE RIGHT MATRICES
		//
		//printf("\nUpdate right at iteration %d\n",i);
		// Solve the equations for updating the right matrix
		updateRight();
		

	}
	
	clock_gettime(CLOCK_MONOTONIC, &t1);
	gettimeofday(&ti2, NULL); /* read endtime in t2 */
    	runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6 * (ti2.tv_usec - ti1.tv_usec);
    	printf("\nTime taken for ALS of size %d:\n%f seconds.\n", cColumns, runtime);
	cudacall(cudaMemcpy(h_leftMat, d_leftMat, cRows * cRows * cOrderARX * cRankKron * sizeof(float), cudaMemcpyDeviceToHost));
	cudacall(cudaMemcpy(h_rightMat, d_rightMat, cColumns * cColumns * cOrderARX * cRankKron * sizeof(float), cudaMemcpyDeviceToHost));
	
}

/**
 * This function updates the left matrices (C).
 *
 * @see updateRight()
 */
void ALS::updateLeft() {
	// Some constants needed for the multiplication
	const float alpha = 1;
	const float beta = 0;
	
	magma_int_t h_info = 0;
	
	//float *buffer = (float *) malloc(cRows * cColumns * nIterations * cRankKron * sizeof(float));

	// For each ARX order
	for(int i = 0, z2 = 0; i < cOrderARX; i++) {
		// For each Kronecker rank
		for(int j = 0; j < cRankKron; j++) {

			// Do the batch matrix multiplication to get:
			// [ u(0) B_r, u(1) B_r, ... , u(k) B_r ]
			// where r is the Kronecker rank or ARX order
			// Note that B = [ B_1'   B_2'  ...  B_r' ] which means we multiply with the transpose 
			cublascall(cublasSgemmBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T, cRows, cColumns, cColumns, &alpha,
					(const float **)&d_ptrSdata[z2 * nIterations], cRows, (const float **)&d_ptrMatB[z2 * nIterations], 
					cColumns, &beta, d_ptrDataTemp, cRows, nIterations));
			
			//cudacall(cudaMemcpy(buffer, d_dataTemp, cRows * cColumns * nIterations * sizeof(float), cudaMemcpyDeviceToHost));
			//printMatrix(buffer, cColumns * nIterations, cRows, 0);
			
			// Transpose the result and store this matrix next to the previous one, to get:
			// [ B_1 u(0)'   B_2 u(0)'   ...  B_r u(0)' ]
			// [ B_1 u(1)'   B_2 u(1)'   ...  B_r u(1)' ]
			// [    :            :                :     ]
			// [ B_1 u(k)'   B_2 u(k)'   ...  B_r u(k)' ]
			cublascall(cublasSgeam(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_T, cColumns * nIterations, cRows, &alpha,
						d_dataTemp, cRows, &beta, d_dataTemp, cRows, 
						&d_data[z2 * cRows * cColumns * nIterations], cColumns * nIterations));
						
			z2++;
		}
	}
	
	cudacall(cudaMemcpy(d_dataTemp, d_leftMatLS, cRows * cColumns * nIterations * sizeof(float), cudaMemcpyDeviceToDevice));


	// Solve the LS problem ( Y(k)' - [B' U(k)'] C' ) for C'
	//cublascall(cublasSgelsBatched(cublasHandle, CUBLAS_OP_N, cColumns * nIterations, cRows * cOrderARX * cRankKron, 
	//				cRows, d_ptrData,
	//				cColumns * nIterations, d_ptrDataTemp, cColumns * nIterations, &h_info,
	//				NULL, 1));
	
	magma_sgels3_gpu(MagmaNoTrans, (magma_int_t) cColumns * nIterations, (magma_int_t) cRows * cOrderARX * cRankKron, 
			(magma_int_t) cRows, d_data, (magma_int_t) cColumns * nIterations,
			d_dataTemp, (magma_int_t) cColumns * nIterations, d_buffer, lworkgpu, &h_info);
	


	// Copy the LS result to the C matrix, this is written to the input d_leftMatLS. The first 
	// (cRows * cOrderARX * cRankKron) rows of d_leftMatLS contain the result for C.
	for(int i = 0; i < cRows; i++) {
		cudacall(cudaMemcpy(&d_data[i * cRows * cOrderARX * cRankKron], &d_dataTemp[i * nIterations * cColumns],
					cRows * cOrderARX * cRankKron * sizeof(float), cudaMemcpyDeviceToDevice));
	}
	
	// Normalize the columns of C
	//normc(cublasHandle, d_data, cRows * cOrderARX * cRankKron, cRows);
	
	// Store the C matrix as a wide matrix:
	// [ C_1   C_2  ...  C_r ]
	cublascall(cublasSgeam(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_T, cRows, cRows * cOrderARX * cRankKron, &alpha, d_data,
				cRows * cOrderARX * cRankKron, &beta,  d_data, cRows * cOrderARX * cRankKron, d_leftMat, cRows));

}

/**
 * This function updates the right matrices (B).
 *
 * @see updateLeft()
 */
void ALS::updateRight() {
	const float alpha = 1;
	const float beta = 0;
	int h_info = 0;

	for(int i = 0, z2 = 0; i < cOrderARX; i++) {
		for(int j = 0; j < cRankKron; j++) {
			// Do the batch matrix multiplication to get:
			// [ u(0)' C_r', u(1)' C_r', ... , u(k)' C_r' ]
			// where r is the Kronecker rank or ARX order
			cublascall(cublasSgemmBatched(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_T, cColumns, cRows, cRows, &alpha,
					(const float **)&d_ptrSdata[z2 * nIterations], cRows, (const float **)&d_ptrMatC[z2 * nIterations], cRows, &beta, d_ptrDataTemp, 
					cColumns, nIterations));
			//cudaThreadSynchronize();
			// Transpose the result and store this matrix next to the previous one, to get:
			// [ C_1 u(0)   C_2 u(0)   ...  C_r u(0) ]
			// [ C_1 u(1)   C_2 u(1)   ...  C_r u(1) ]
			// [    :            :              :    ]
			// [ C_1 u(k)   C_2 u(k)   ...  C_r u(k) ]
			cublascall(cublasSgeam(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_T, cRows * nIterations, cColumns, &alpha,
						d_dataTemp, cColumns, &beta, d_dataTemp, cColumns, 
						&d_data[z2 * cRows * cColumns * nIterations], cRows * nIterations));
						
			z2++;
		}
	}
	
	//cudaThreadSynchronize();
	
	cudacall(cudaMemcpy(d_dataTemp, d_rightMatLS, cRows * cColumns * nIterations * sizeof(float), cudaMemcpyDeviceToDevice));


	// Solve the LS problem ( Y(k) - [C U(k)] B ) for B
	//cublascall(cublasSgelsBatched(cublasHandle, CUBLAS_OP_N, cRows * nIterations, cColumns * cOrderARX * cRankKron, 
	//				cColumns, d_ptrData,
	//				cRows * nIterations, d_ptrDataTemp, cRows * nIterations, &h_info,
	//				NULL, 1));

	magma_sgels3_gpu(MagmaNoTrans, (magma_int_t) cRows * nIterations, (magma_int_t) cColumns * cOrderARX * cRankKron, 
			(magma_int_t) cColumns, d_data, (magma_int_t) cRows * nIterations,
			d_dataTemp, (magma_int_t) cRows * nIterations, d_buffer, lworkgpu, &h_info);

	// Copy the LS result to the B matrix
	for(int i = 0; i < cColumns; i++) {
		cudacall(cudaMemcpy(&d_data[i * cColumns * cOrderARX * cRankKron], &d_dataTemp[i * nIterations * cRows],
					cColumns * cOrderARX * cRankKron * sizeof(float), cudaMemcpyDeviceToDevice));
	}
	
	// Store the B matrix as a wide matrix:
	// [ B_1'   B_2'  ... B_r' ]
	cublascall(cublasSgeam(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_T, cColumns, cColumns * cOrderARX * cRankKron, &alpha, d_data,
				cColumns * cOrderARX * cRankKron, &beta,  d_data, cColumns * cOrderARX * cRankKron, d_rightMat, cColumns));
}

// Create the wrapper functions for this library, these will be needed in the Python wrapper to access the functions
extern "C" {
	ALS *ALS_new(int pRows, int pColumns, int pIterations, int pOrderARX, int pALSIter, int pRankKron) { 
		return new ALS(pRows, pColumns, pIterations, pOrderARX, pALSIter, pRankKron);
	}
	
	void ALS_destroy(ALS *als) { delete als; }
	
	void ALS_runALS(ALS *als) { als->runALS(); }
	
	void ALS_loadTextData(ALS *als, const char *sDataLoc, const char *uDataLoc, const char *leftMatLSLoc, const char *rightMatLSLoc) {
		als->loadData(sDataLoc, uDataLoc, leftMatLSLoc, rightMatLSLoc);
	}
	
	void ALS_loadData(ALS *als, float *sData, float *uData, float *leftMatLS, float *rightMatLS) {
		als->loadData(sData, uData, leftMatLS, rightMatLS);
	}
	
	void ALS_saveTextData(ALS *als, const char *leftMatLoc, const char *rightMatLoc) {
		als->saveData(leftMatLoc, rightMatLoc);
	}
	
	void ALS_saveData(ALS *als, float *leftMat, float *rightMat) {
		als->saveData(leftMat, rightMat);
	}
}
