/* ALS_magma.cuh
 *
 * Header file for the ALS solver library using the MAGMA library.
 * Note: You need to have the MAGMA library installed to be able to use this.
 *
 * G. Monchen (2017)
 */

#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <magma.h>


#include <assert.h>

#ifndef ALS_H
#define ALS_H

/**
 * The Alternating Least Squares solver class.
 * 
 * This class can solve identification problems for models of the form:
 * 	Y(k) = C Y(k-1) B + V(k)
 * where you can choose any arbitrary Kronecker rank and/or ARX order.
 *
 * When using the included Make file, this class is build as a library
 * which accepts the calls for any of the public functions defined here.
 *
 * For an example on using this library, refer to main.py.
 */
class ALS {
public:
	// Constructor
	ALS(int pRows, int pColumns, int pIterations, int pOrderARX, int pALSIter, int pRankKron);
	// Destructor
	~ALS();
	
	// Run the ALS algorithm
	void runALS();
	
	// Load/Save data
	void loadData(const char *sDataLoc, const char *uDataLoc, const char *leftMatLSLoc, const char *rightMatLSLoc);
	void loadData(float *sData, float *uData, float *leftMatLS, float *rightMatLS);
	void saveData(const char *leftMatLoc, const char *rightMatLoc);
	void saveData(float *leftMat, float *rightMat);
	
private:
	int cRows;							//!< Number of rows of Y(k)
	int cColumns;						//!< Number of columns of Y(k)
	int cIterations;					//!< Total number of iterations
	int cOrderARX;						//!< ARX Order
	int cALSIter;						//!< Number of ALS iterations to perform
	int cRankKron;						//!< Kronecker rank, this is the same for all ARX orders
	int nIterations;					//!< Number of usable iterations for identification, equal to cIterations - cOrderARX
	
	// Host variables
	float *h_sData;						//!< Host pointer to Y(k) in vectorized form
	float *h_uData;
	float *h_leftMatLS;					//!< Host pointer to the left side of the LS problem with Y(k)' for estimating C
	float *h_rightMatLS;				//!< Host pointer to the left side of the LS problem with Y(k) for estimating B
	float *h_leftMat;	 				//!< Host pointer to the left matrix result (C)
	float *h_rightMat;					//!< Host pointer to the right matrix result (B)
	
	// Device variables
	float *d_sData;						//!< Device pointer to Y(k) in vectorized form
	float *d_uData;
	float *d_leftMatLS;					//!< Device pointer to the left side of the LS problem with Y(k)' for estimating C
	float *d_rightMatLS;				//!< Device pointer to the left side of the LS problem with Y(k) for estimating B
	float *d_leftMat;					//!< Host pointer to the left matrix result (C)
	float *d_rightMat;					//!< Host pointer to the right matrix result (B)
	
	float *d_data;
	float *d_dataTemp;
	float *d_buffer;
	float *d_tau;
	
	float **d_ptrMatB, **d_ptrSdata, **h_ptrMatB, **h_ptrSdata;
	float **d_ptrMatC, **h_ptrMatC;
	float **d_ptrDataTemp, **h_ptrDataTemp;
	float **d_ptrData, **h_ptrData;
	int *d_devInfo;
	
	magma_int_t lworkgpu;
	magma_int_t ldda, lddb;
	
	// CUBLAS handle
	cublasHandle_t cublasHandle;		//!< Handle to the CUBLAS library
#ifdef CUSOLVER
	cusolverDnHandle_t cusolverHandle;	//!< Handle to the cudsolver library
#endif
	
	// Private functions
	void updateLeft();
	void updateRight();
};

#endif
