.SUFFIXES : .cl

# detect OS
OSUPPER = $(shell uname -s 2>/dev/null | tr [:lower:] [:upper:])
OSLOWER = $(shell uname -s 2>/dev/null | tr [:upper:] [:lower:])

# detect if 32 bit or 64 bit system
HP_64 =	$(shell uname -m | grep 64)
OSARCH= $(shell uname -m)

CUDAROOT := /opt64/cuda
SDKDIR := /home/dv/lemmens/Parallel/OpenCL/OCLExamples/SDK

# Basic directory setup for SDK
# (override directories only if they are not already defined)
LIBDIR     := $(SDKDIR)/shared/lib/
SHAREDDIR  := $(SDKDIR)/shared/
OCLROOTDIR := $(SDKDIR)/OpenCL/
OCLCOMMONDIR ?= $(OCLROOTDIR)/common/
OCLBINDIR  ?= $(OCLROOTDIR)/bin/
OCLLIBDIR  := $(OCLCOMMONDIR)/lib

# Compilers
CXX        := g++
CC         := gcc
LINK       := g++ -fPIC

# Includes
INCLUDES  += -I. -I$(OCLCOMMONDIR)/inc -I$(SHAREDDIR)/inc -I$(CUDAROOT)/samples/common/inc

ifeq "$(strip $(HP_64))" ""
	MACHINE := 32
	USRLIBDIR := -L/usr/lib/
else
	MACHINE := 64
	USRLIBDIR := -L/usr/lib64/
endif

# Warning flags
CXXWARN_FLAGS := \
	-W -Wall \
	-Wimplicit \
	-Wswitch \
	-Wformat \
	-Wchar-subscripts \
	-Wparentheses \
	-Wmultichar \
	-Wtrigraphs \
	-Wpointer-arith \
	-Wcast-align \
	-Wreturn-type \
	-Wno-unused-function \
	-std=c++11 \
	$(SPACE)

CWARN_FLAGS := $(CXXWARN_FLAGS) \
	-Wstrict-prototypes \
	-Wmissing-prototypes \
	-Wmissing-declarations \
	-Wnested-externs \
	-Wmain \


# architecture flag for nvcc and gcc compilers build
LIB_ARCH        := $(OSARCH)

# Compiler-specific flags
CXXFLAGS  := $(CXXWARN_FLAGS) $(CXX_ARCH_FLAGS)
CFLAGS    := $(CWARN_FLAGS) $(CXX_ARCH_FLAGS)
LINK      += $(CXX_ARCH_FLAGS)

# Common flags
COMMONFLAGS += $(INCLUDES) -DUNIX

# Debug/release configuration
ifeq ($(dbg),1)
	COMMONFLAGS += -g
	LIBSUFFIX   := D
else 
	COMMONFLAGS += -O3 
	LIBSUFFIX   :=
	CXXFLAGS    += -fno-strict-aliasing
	CFLAGS      += -fno-strict-aliasing
endif

# OpenGL is used or not (if it is used, then it is necessary to include GLEW)
ifeq ($(USEGLLIB),1)

	OPENGLLIB := -lGL -lGLU -lX11 -lXmu
	ifeq "$(strip $(HP_64))" ""
		OPENGLLIB += -lGLEW -L/usr/X11R6/lib
	else
		OPENGLLIB += -lGLEW_x86_64 -L/usr/X11R6/lib64
	endif

	CUBIN_ARCH_FLAG := -m64
endif

ifeq ($(USEGLUT),1)
       	OPENGLLIB += -lglut
endif

# Libs
LIB := ${USRLIBDIR} -L${OCLLIBDIR} -L$(LIBDIR) -L$(SHAREDDIR)/lib/$(OSLOWER) 
LIB += ${OPENGLLIB} ${LIB} ${OCLLIBDIR}/liboclUtil_x86_64.a -lOpenCL 

# Lib/exe configuration
ifneq ($(STATIC_LIB),)
	TARGETDIR := .
	TARGET   := $(subst .a,_$(LIB_ARCH)$(LIBSUFFIX).a,$(OCLLIBDIR)/$(STATIC_LIB))
	LINKLINE  = ar qv $(TARGET) $(OBJS) 
else
	LIB += -loclUtil_$(LIB_ARCH)$(LIBSUFFIX) 
	LIB += ../Common/libshrutil.a # -lshrutil_$(LIB_ARCH)$(LIBSUFFIX)
	TARGETDIR := .
	TARGET    := $(TARGETDIR)/$(EXECUTABLE)
	LINKLINE  = $(LINK) -o $(TARGET) $(OBJS) $(LIB)
endif

VERBOSE :=
#VERBOSE := @

# Add common flags
CXXFLAGS  += $(COMMONFLAGS)
CFLAGS    += $(COMMONFLAGS)


################################################################################
# Set up object files
################################################################################
OBJDIR := .
OBJS +=  $(patsubst %.cpp,%.cpp.o,$(notdir $(CCFILES)))
OBJS +=  $(patsubst %.c,%.c.o,$(notdir $(CFILES)))

################################################################################
# Rules
################################################################################
%.c.o : %.c $(C_DEPS)
	$(VERBOSE)$(CC) $(CFLAGS) -o $@ -c $<

%.cpp.o : %.cpp $(C_DEPS)
	$(VERBOSE)$(CXX) $(CXXFLAGS) -o $@ -c $<

$(TARGET): $(OBJS) Makefile
	$(VERBOSE)$(LINKLINE)

tidy :
	$(VERBOSE)find . | egrep "#" | xargs rm -f
	$(VERBOSE)find . | egrep "\~" | xargs rm -f

clean : tidy
	$(VERBOSE)rm -f $(OBJS) *.o *.ptx core
	$(VERBOSE)rm -f $(TARGET)

