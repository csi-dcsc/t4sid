function [ THETAk1, PK1 ] = rls( yk, phik, thetak, pk, lmb )
%RLS Solve a recursive least squares (RLS) problem.
%  yk = phik * thetak
%  yk'= thetak * phik => yk = phik' * thetak'
%   
% INPUT:
% yk: the output variable
% phik: the input variable
% thetak: the coefficient vector
% pk: the P(k) matrix
% lmb: the forgetting parameter
%
% Guido Monchen, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

LF = pk * phik' / (lmb * eye(size(yk,1)) + phik * pk * phik');
THETAk1 = thetak + LF * (yk - phik * thetak);
PK1 = (1/lmb) * (pk - LF * phik * pk);

end

