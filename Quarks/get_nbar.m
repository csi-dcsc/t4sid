function n = get_nbar(v)
% Computes the number of columns for each unfolding of a tensor.
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(v);
n = zeros(d,1);
for x = 1:d
    if x == 1
        n(x) = prod(v(2:end));
    elseif x == d
        n(x) = prod(v(1:end-1));
    else
        n(x) = prod([v(1:x-1) v(x+1:end)]);
    end
end

end