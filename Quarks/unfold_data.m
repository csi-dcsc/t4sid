function [Y_UNFOLDED,N] = unfold_data(d,v,a,b,y)
% Unfold the data contains in y into d matrices 
%
% INPUT:
% d: tensor order
% v: row vector containing the dimensions of one time occurence of y
% a: starting index of the cell storing y
% b: last index of the cell storing y
% y: the cell containing e.g the output data in the QUARKS
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

Y_UNFOLDED = cell(d,1);
N = zeros(d,1);
for x = 1:d
    if x == 1
        N(x) = prod(v(2:end));
    elseif x == d
        N(x) = prod(v(1:end-1));
    else
        N(x) = prod([v(1:x-1) v(x+1:end)]);
    end
    Y_UNFOLDED{x} = zeros(v(x),b*N(x));
    for k = a:b
        Y_UNFOLDED{x}(:,(k-a)*N(x)+1:(k-a+1)*N(x)) = tens2mat(y{k},d-x+1);
    end
end

end
