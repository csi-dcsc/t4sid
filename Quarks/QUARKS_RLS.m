function [M_HAT,ITER,CV] = QUARKS_RLS(param,Uid,Yid,s,forgetting_factor)
% Recursive estimation of Kronecker AutoRegressive models
% Example of possible model structures: Finite Impulse Responses, AutoRegressive
% 
% INPUT:
% the struct param contains (among others):
%   J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
%   I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
% Uid: cell Ntx1, contains the input data
% Yid: cell Ntx1, contains the output data
% M: cell of size sxrxd (s: temporal order, r: Kronecker rank, d: tensor order), containing the real factor matrices for evaluating the quality of the estimation
% forgetting_factor: forgetting factor < 1, the lower, the faster past data is forgotten.
% OUTPUT: 
% M_hat: cell sxrxd, containing the estimated factor matrices
% iter: matrix of size Nidx2, containing accuracy results
% cv: vector of size Nid, containing in each entry the Frobenius norm of the difference between two consecutives factor matrices 
%
% Guido Monchen, April 2017
% Revised by Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

Nid = length(Uid);
r = param.r;
J = fliplr(param.J);
I = fliplr(param.I);
d = length(I);
M_HAT = cell(s,r,d);
% initial guesses
for i = 1:s
    for x = 1:d  
        for j = 1:r
            M_HAT{i,j,x} = randn(J(x),I(x));
        end
    end
end
% initialize covariance matrices
delta = 1;
P = cell(d,1);
nbar = zeros(d,1);
for x = 1:d
    P{x} = delta * eye(s*r*J(x));
    if x == 1
        nbar(x) = prod(J(2:end));
    elseif x == d
        nbar(x) = prod(J(1:end-1));
    else
        nbar(x) = prod([J(1:x-1) J(x+1:end)]);
    end
end
% start algorithm
ITER = zeros(Nid,2);
CV = zeros(Nid,1);
k = s+1;
while k < Nid 
    Yhat = zeros(fliplr(J));
    for i = 1:s
        for j = 1:r
            Yhat = Yhat+tmprod(Uid{k-i},{M_HAT{i,j,:}},d:-1:1);
        end 
    end
    ITER(k,1) = norm(Yhat(:)-Yid{k}(:),2)/norm(Yid{k}(:),2);
    % 
    for x = 1:d
        %                        
        yk = tens2mat(Yid{k},d-x+1)';
        phik = zeros(s*r*J(x),nbar(x));
        modes = [1:x-1 x+1:d];
        id = 1;
        for i = 1:s
            for j = 1:r
                temp = tmprod(permute(Uid{k-i},d:-1:1),{M_HAT{i,j,modes}},modes);
                phik((id-1)*J(x)+1:id*J(x),:) = reshape(permute(temp,[x [d:-1:x+1] [x-1:-1:1]]),I(x),nbar(x));
                id = id+1;
            end
        end 
        thetak = zeros(J(x),J(x)*s*r);
        id = 1;
        for i = 1:s
            for j = 1:r
                thetak(:,(id-1)*J(x)+1:id*J(x)) = M_HAT{i,j,x}; 
                id = id+1;
            end
        end
        %
        old = thetak';
        [thetak, P{x}] = rls( yk, phik', thetak', P{x}, forgetting_factor );
        CV(k,x) = norm(old-thetak,'fro');
        thetak = thetak'; 
        id = 1;
        for i = 1:s
            for j = 1:r
                M_HAT{i,j,x} = thetak(:,(id-1)*J(x)+1:id*J(x));      
                id = id+1;
            end
        end        
    end
    %            
    k = k+1;
end
                    

end
