function [M_hat,residual_norm] = quarks(param,Uid,Yid,s,opt)
% Alternating Least Squares for estimating the QUARKS
% Example of possible model structures that it can handle: Finite Impulse Responses, AutoRegressive
% 
% INPUT:
% the struct param contains:
%   I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
%   J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
%   r: Kronecker rank
%   d: tensor order
% Uid: cell Ntx1, contains the input data
% Yid: cell Ntx1, contains the output data
% s: temporal order
% the struct opt contains: 
%   pos: scalar. if set to 1, the factor matrices are strictly positive elementwise. else, random matrices
% OUTPUT: 
% M_hat: cell sxrxd, containing the estimated factor matrices
% residual_norm: vector of size itermax (maximum number of iterations in the ALS), containing the residuals in Frobenius norm 
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

I = fliplr(param.I);
J = fliplr(param.J);
r = param.r;
d = length(I);

Nid = length(Yid);
% default values
std = 1; banded = 0; pos = 0;
if exist('opt','var') && isfield(opt,'struct')
    if strcmp(opt.struct,'std') == 1, std = 1; banded = 0; pos = 0; end, 
    if strcmp(opt.struct,'banded') == 1, std = 0; banded = 1; pos = 0; end
    if strcmp(opt.struct,'positive') == 1, std = 0; banded = 0; pos = 1; end
end
if exist('opt','var') && isfield(opt,'lambda'), lambda = opt.lambda; else lambda = 0; end
if exist('opt','var') && isfield(opt,'bandwidth'), bandwidth = opt.bandwidth; else bandwidth = 7; end
%
if exist('opt','var') && isfield(opt,'itermax'), itermax = opt.itermax; else itermax = 20; end
if exist('opt','var') && isfield(opt,'gradmin'), gradmin = opt.gradmin; else gradmin = 1e-3; end
if exist('opt','var') && isfield(opt,'normalize'), normalize = opt.normalize; else normalize = 1e-3; end
if d > 2, normalize = 0; end
if exist('opt','var') && isfield(opt,'normalize'), normalize = opt.normalize; else normalize = 1e-3; end
if banded == 1
    x = 1;
    P = get_pattern(J(x),I(x),bandwidth);
    pattern = kron(ones(1,s*r),P);
end

%% 
M_hat = cell(s,r,d);
% initial guesses
for i = 1:s
    for j = 1:r
        for x = 1:d        
            M_hat{i,j,x} = abs(randn(J(x),I(x)));
        end
    end
end
% unfolded outputs
[Y_unfolded,nbar] = unfold_data(d,J,s+1,Nid,Yid);
% start iterations
residual_norm = zeros(itermax,2);
iter = 1;
old = Inf;
grad = Inf;
while iter < itermax  && grad > gradmin
    for x = 1:d
        modes = [1:x-1 x+1:d];
        mat = zeros(I(x)*s*r,Nid*nbar(x));
        for k = s+1:Nid
            for i = 1:s
                for j = 1:r
                    tmp = tmprod(permute(Uid{k-i},d:-1:1),{M_hat{i,j,modes}},modes);
                    mat((i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x),(k-s-1)*nbar(x)+1:(k-s)*nbar(x)) = ...
                                reshape(permute(tmp,[x [d:-1:x+1] [x-1:-1:1]]),I(x),nbar(x));
                end
            end
        end
        if std == 1
            temp = Y_unfolded{x}*mat'/((mat*mat'+lambda*eye(I(x)*s*r)));
        elseif banded == 1
            for ii = 1:J(x)
                MAT = [];
                for i = 1:s
                    for j = 1:r
                        pp = mat((i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x),:);
                        MAT = [MAT;pp(find(P(ii,:)),:)];                        
                    end
                end     
                temp(ii,find(pattern(ii,:))) = Y_unfolded{x}(ii,:)*MAT'/((MAT*MAT'+lambda*eye(size(MAT,1))));
            end
        elseif pos == 1
            maxiterP = 200;
            % using ADMM
            imat = inv(mat*mat'+lambda*eye(I(x)*s*r));
            N = imat*mat*Y_unfolded{x}';
            uk = zeros(I(x)*s*r,J(x));
            xk = uk;
            zk = uk;
            iterP = 1; normrp = Inf;
            while iterP < maxiterP && normrp > 1e-3
                xk = N+imat*(zk-uk);
                zk = max(0,xk+uk);
                uk = uk+xk-zk;    
                normrp = norm(xk-zk,'fro');                
                iterP = iterP+1;
            end
            temp = xk';
        end
        %
        for i = 1:s
            for j = 1:r
                M_hat{i,j,x} = temp(:,(i-1)*I(x)*r+(j-1)*I(x)+1:(i-1)*I(x)*r+j*I(x));
                if normalize == 1
                    if x == 1
                        if fixedpoint == 0
                            M_hat{i,j,x} =  normc(M_hat{i,j,x}); 
                        elseif fixedpoint == 1
                            for jj = 1:size(M_hat{i,j,x},2)
                                M_hat{i,j,x} = M_hat{i,j,x}/norm(M_hat{i,j,x}(:,jj),2)*norm(M_{i,j,x}(:,jj),2); 
                            end
                        end
                    end
                end
            end
        end
    end    
    new = norm(Y_unfolded{x}-temp*mat,'fro');
    grad = abs(new-old);
    old = new;
    residual_norm(iter,1) = new/norm(Y_unfolded{x},'fro');
    if normalize == 1
        residual_norm(iter,2) = norm(M_hat{i,j,1}-M{i,j,1},'fro');
    end
    %
    iter = iter+1;
end

end
