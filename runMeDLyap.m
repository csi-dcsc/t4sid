% Solve the Lyapunov equation written in discrete time: A*X*A' - X + Q = 0
% where A = A_d \otimes ... \otimes A_1
%
% Reference: 
% R. Smith, �Matrix equation XA+BX = C,� SIAM Journal of Applied Mathematics,
% vol. 16, p. No. 1, 1968
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

clc; clear; close all

% generate the matrices
params = [];
params.r = 1;                 
params.I = [20 20];            
N = prod(params.I);
params.J = params.I;           
params.n = params.J;           
params.d = length(params.I);    
% Important note: the Kronecker rank may be increased in which case it is
% necessary to play with the values of threshold or spectral radius of each
% factor matrix to form a full matrix which is stable, without computing
% many eigenvalue decompositions of the latter especially when the sizes
% are large.
opt.struct = 'SSS';
opt.decay = 0.98;
sys = SOK_ss(params,opt); 
sigma = 1e-1;
Q = SOK_spdmat(params,sigma);
% first dealing with unstructured matrices 
ub = 1e4;
if N < ub
    Ag = SOK_getmat(sys.A);
    Qg = SOK_getmat(Q);
    figure,imagesc(abs(Ag)),axis square,colorbar,colormap bone,
    figure,imagesc(abs(Qg)),axis square,colorbar,colormap bone,
    drawnow
    % with Matlab function
    tic
    P_m = dlyap(Ag,Qg);
    time_matlab = toc;
    im(P_m)
    % with doubling algorithm,
    tic
    Y = compute_dlyap(Ag,Qg);
    time_unstructured = toc;
end
% then, exploiting the Kronecker structure
% using a doubling algorithm
ra = 2;
method = 'Smith';
tic
P = SOK_dlyap(sys.A,Q,ra,method);
SOK_time = toc;
if N < ub
    P_k = SOK_getmat(P);
    residual = rmse(P_k,P_m);
    fprintf('Relative RMSE between unstructured solution and Kronecker-based: %6.2f\n',residual);
    fprintf('(Time SOK)/(Min unstructured): %6.2f\n',SOK_time/min(time_unstructured,time_matlab));
end


