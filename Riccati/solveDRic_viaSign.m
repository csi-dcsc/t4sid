function K = solveDRic_viaSign(A,B,Q,R)

n = size(A,1);
F = A;
G1 = B;
G2 = R;
G = G1*inv(G2)*G1';
H = Q;
% apply bilinear transform
N = [F zeros(n);-H eye(n)];
L = [eye(n) G;zeros(n) F'];
Ha = (N+L)\(N-L); 
% compute matrix sign
W = compute_matrix_sign(Ha);
% solve least-squares
left = [W(1:n,n+1:2*n);W(n+1:2*n,n+1:2*n)+eye(n)];
right = -[W(1:n,1:n)+eye(n);W(n+1:2*n,1:n)];
X_sign = pinv(left)*right;
% get Kalman gain
K = (B'*X_sign*B+R)\(B'*X_sign*A);

end