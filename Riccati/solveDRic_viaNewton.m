function K = solveDRic_viaNewton(A,B,Q,R)

n = size(A,1);
R_op = @(X) Q+A'*X*A-X-(A'*X*B)*inv(R+B'*X*B)*(A'*X*B)';
K_op = @(X) (A'*X*B)*inv(R+B'*X*B);
%
X = eye(n);
itermax = 100;
iter = 1;
epsilon = 1e-6;
temp = Inf;
%residual = zeros(itermax,1);
while iter < itermax && temp > epsilon
    Kk = K_op(X);
    Ak = A'-Kk*B';
    %max(abs(eig(Ak)))
    Rk = R_op(X);
    Nk = dlyap(Ak,Rk);
    X = X+Nk;
    % 
    %temp = norm(Nk,'fro');
    %residual(iter,1) = temp;
    % 
    iter = iter+1;
end
K = Kk;
%figure,semilogy(residual)

end