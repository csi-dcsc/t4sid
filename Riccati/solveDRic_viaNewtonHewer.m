function K = solveDRic_viaNewtonHewer(A,B,Q,R,TRUE)

n = size(A,1);
C_op = @(K) Q+K*R*K';
K_op = @(X) (A'*X*B)*inv(R+B'*X*B);
X = eye(n);
%
X_old = X;
itermax = 100;
iter = 1;
epsilon = 1e-6;
temp = Inf;
%residual = zeros(itermax,1);
while iter < itermax && temp > epsilon
    Kk = K_op(X);
    Ak = A'-Kk*B';
    Ck = C_op(Kk);
    X = dlyap(Ak,Ck);
    % 
    temp = norm(X-X_old,'fro');
    %residual(iter,1) = temp;
    %residual(iter,2) = get_rmse(Kk,TRUE);
    X_old = X;
    % 
    iter = iter+1;
end
K = Kk;
%figure,semilogy(residual)

end