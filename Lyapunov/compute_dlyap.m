function [Y] = compute_dlyap(Ag,Qg)
% Compute the solution to the discrete Lyapunov equation using the Smith's
% iteration
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

U = Ag;
V = U';
Y = Qg;
N = size(Y,1);
Y_old = zeros(N);
itermax = 1e2; 
iter = 1;
temp = Inf;
while iter < itermax && temp > 1e-6
    Y = Y+U*Y*V;
    U = U^2;
    V = U';
    temp = norm(Y-Y_old,'fro');
    Y_old = Y;
    iter = iter+1;
end
    
end