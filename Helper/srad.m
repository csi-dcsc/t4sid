function rho = srad(X)
% Compute the spectral radius of the matrix X
%
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

rho = max(abs(eig(X)));

end