function [ H ] = deformableMirror( m,o,D,coupling,id )
%DEFORMABLEMIRROR Create Deformable Mirror matrix H
%   H = deformableMirror(m,n,F,sigma), returns the matrix M in the model for
%   the disturbance wavefront induced by the deformable mirror given as:
%
%   phi_dm(k) = M * u(k-1)
%
%   For the parameters:
%
%       m           Number of phase screen points in one dimension
%       o           Number of actuators in one dimension
%       D           Diameter of telescope
%       coupling    Coupling from one actuator to its neighbour
%       id          Choice for the influence function
%
%   Where the Gaussian influence function is defined as:
%       exp( -d / sigma^2 )
%
%   This function has only been implemented for square phase screens with
%   the actuators evenly distributed with dimensions (o x o).
%
% Guido Monchen, April 2017 
% Copyright (c) 2017, Delft Center of Systems and Control 

H = zeros(m^2,o^2);

% First calculate the spatial locations of the measured phase points and
% the actuator points based on the size of the aperture
dm = D/(m+1);
dN = D/(o+1);

sigma = sqrt(-dN/log(coupling));

pm = dm:dm:(D-dm);
pN = dN:dN:(D-dN);

% Loop through each phase point and calculate the influence of each
% actuator on that specific phase point
for i=1:m
    for j=1:m
        Mtemp = zeros(o,o);
        for k=1:o
            for l=1:o
                % Calculate the stationary delta distance
                dx = pN(k) - pm(i);
                dy = pN(l) - pm(j);
                d = sqrt(dx^2 + dy^2);
                if id == 1
                    Mtemp(k,l) = exp(-d^2/(sigma^2));
                elseif id == 2
                    Mtemp(k,l) = exp(-d/(sigma^2));
                end
            end
        end

        % Populate the final matrices with the temporary ones we created
        H(i+(j-1)*m,:)=(Mtemp(:))';
    end
end

end

