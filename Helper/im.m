function im(X)
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

figure,imagesc(abs(X)),colormap bone,axis square,colorbar
set(gca,'Fontsize',12.5)
drawnow

end