function get_pattern(row,col,bandwidth)

P = zeros(row,col);
for i = 1:row
   for j = 1:col
       if abs(i-j) < bandwidth
           P(i,j) = 1;
       end
   end
end
   
end