function VAF = get_vaf(Yref,Yhat)
% Compute the Variance Accounted For between two signals stored in cells
% Yref and Yhat
% 
% INPUT:
% Yref: cell Ntx1 (Nt is the number of temporal samples in the dataset), reference output
% Yhat: cell Ntx1, estimated output
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% OUTPUT:
% VAF: vector of size prod(J)x1, contains the VAF for each output channel between the original and estimated data
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

Nt = length(Yref);
J = size(Yref{2});
vecYref = zeros(prod(J),Nt);
vecYhat = zeros(prod(J),Nt);
for k = 1:Nt
    vecYref(:,k) = vec(Yref{k});
    vecYhat(:,k) = vec(Yhat{k}); 
end

VAF = vaf(vecYref(:),vecYhat(:));

end
