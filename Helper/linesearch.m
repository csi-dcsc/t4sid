function t = linesearch(fk,d,x,t0,rho,c)
% function alphak = linesearch(f,d,x,rho,c)
% Backtracking line search
% See Algorithm 3.1 on page 37 of Nocedal and Wright
% Input Parameters :
% f: MATLAB file that returns function value
% d: The search direction
% x: previous iterate
% rho: The backtrack step between (0,1) usually 1/2
% c: parameter between 0 and 1 , usually 10^{-4}
% Output :
% t: step length calculated by algorithm
% Kartik's MATLAB code (27/1/08)

t = t0;
f = fk(x);
xx = x;
x = x + t*d;
fk1 = fk(x);
while fk1 > f + c*t*sum(d),
  t = t*rho;
  x = xx + t*d;
  fk1 = fk(x);
end
