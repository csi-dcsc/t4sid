function [SSS]= SSS_findupper(fullmatrix, N, n_r, n_c, n_w)
% This will take a  full matrix, and find the SSS generators - this idae
% comes from Shiv's paper
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

SSS.N=N;
SSS.n_r=n_r;
SSS.n_c=n_c;
SSS.n_w = n_w;

B=zeros(n_r, n_w, N);
W=zeros(n_w, n_w, N);
C=zeros(n_w, n_c, N);

H=fullmatrix(1:n_r, 1+n_c:n_c*N);
[left, middle, rightT]=svd(H); right=rightT';
cow=n_w-size(middle,1);

left=[left, zeros(size(left,1), cow)];
middle=[middle, zeros(size(middle,1), cow); zeros(cow, size(middle,2)), zeros(cow, cow)];
right=[right; zeros(cow, size(right, 2))];

B(:,:,1)=left(1:n_r, 1:n_w);

C(:,:,2)=middle(1:n_w, 1:n_w)*right(1:n_w,1:n_c);
top=middle(1:n_w, 1:n_w)*right(1:n_w, n_c+1: size(right, 2));

for i=2:N-1
    
    H=[top; fullmatrix(1+ (i-1)*n_r:i*n_r,  1+i*n_c:n_c*N) ];
    [left, middle, rightT]=svd(H);
    right=rightT';
    left=[left, zeros(size(left,1), cow)];
    middle=[middle, zeros(size(middle,1), cow); zeros(cow, size(middle,2)), zeros(cow, cow)];
    right=[right; zeros(cow, size(right, 2))];

    W(:,:,i)=left(1:n_w, 1:n_w);

    B(:,:,i)=left(1+n_w:size(left,1), 1:n_w);
 
    if(n_w>min(size(middle)))
         blah=zeros(size(middle)+[1,1]);
         blah(1:size(middle,1),1:size(middle,2))=middle;
         middle=blah;

         blah=zeros(size(right)+[1,1]);
         blah(1:size(right,1),1:size(right,2))=right;
         right=blah;
    end
 
    C(:,:,i+1)=middle(1:n_w,1:n_w)*right(1:n_w,1:n_c);

    top=middle(1:n_w,1:n_w)*right(1:n_w, n_c+1: size(right, 2));

end


B(:,:,i+1)=0*B(:,:,i);
W(:,:,i+1)=0*W(:,:,i);


for i = 1:N
    SSS.U{i} = B(:,:,i);
    SSS.W{i} = W(:,:,i);
    SSS.V{i} = C(:,:,i);
    %
    SSS.P{i} = zeros(n_r, 1);
    SSS.Q{i} = zeros(1, n_c);
    SSS.R{i} = zeros(1,1);
    %
    SSS.D{i}=zeros(n_r, n_c);
end


end
