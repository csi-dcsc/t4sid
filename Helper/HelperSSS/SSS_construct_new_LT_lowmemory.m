function SSS_mat = SSS_construct_new_LT_lowmemory(SSS)
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach", 
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

P=SSS.P;
R=SSS.R;
Q=SSS.Q;
D=SSS.D;
N=SSS.N;
n_r=SSS.n_r;
n_c=SSS.n_c;
n_w=SSS.n_w;

n_lines = sum(n_r(1:N));
n_columns = sum(n_c(1:N));  
SSS_mat = zeros(n_lines,n_columns);
for k = 2:N   
	r = sum(n_r(1:k-1))+1;
	c = sum(n_c(1:(k-1)-1))+1;
    SSS_mat(r:r+n_r(k)-1,c:c+n_c(k-1)-1) = P{k}*Q{k-1};
end

% now initialize Rstoragecontainer
IR = cell(N);
for i = 1:N
	IR{i} = eye(size(R{i},1));
end
Rstoragecontainer = cell(N);
for i = 1:N-1
	Rstoragecontainer{i} = IR{i};
end

% now get down to business of filling in
for k = 2:N %k is the diagonal counter
    z = 1;
    for i = k:N-1
        newRstoragecontainer{z} = R{i}*Rstoragecontainer{z};
        row = sum(n_r(1:(i+1)-1))+1;
        column = sum(n_c(1:(i-(k-1))-1))+1;
        SSS_mat(row:row+n_r(i+1)-1,column:column+n_c(i-(k-1))-1) = P{i+1}*newRstoragecontainer{z}*Q{i-(k-1)};
        z = z+1;
    end
    Rstoragecontainer = newRstoragecontainer;
end


end

