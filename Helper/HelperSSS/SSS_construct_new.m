function SSS_mat = SSS_construct_new(SSS)
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach",
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

D = SSS.D;

N = SSS.N;
n_r = SSS.n_r;
n_c = SSS.n_c;
n_w = SSS.n_w;

SSSLT = SSS_construct_new_LT_lowmemory(SSS);
SSSUT = SSS_construct_new_LT_lowmemory(SSS_transpose(SSS))';
SSS_mat = SSSLT+SSSUT;

% fill in the diagonal first 
for i = 1:N
	% locate in the matrix
	r = sum(n_r(1:i-1))+1;
	c = sum(n_c(1:i-1))+1;
	%
	SSS_mat(r:r+n_r(i)-1,c:c+n_c(i)-1) = D{i};
end


