function [SSSB]=SSS_transpose(SSSA)
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach",
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

for i = SSSA.N:-1:1
    SSSB.D{i} = SSSA.D{i}';

    SSSB.Q{i} = SSSA.U{i}';
    SSSB.P{i} = SSSA.V{i}';
    SSSB.R{i} = SSSA.W{i}';

    SSSB.W{i} = SSSA.R{i}';
    SSSB.V{i} = SSSA.P{i}';
    SSSB.U{i} = SSSA.Q{i}';
end

SSSB.n_c = SSSA.n_r;
SSSB.n_r = SSSA.n_c;
SSSB.n_w = SSSA.n_w;
SSSB.N = SSSA.N;

end