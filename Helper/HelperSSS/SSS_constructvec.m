function SSS_mat = SSS_constructvec(SSS)
% Reference: J. Rice, "Efficient Algorithms for Distributed Control: A Structured Matrix Approach", 
% PhD thesis, TU Delft, 2010.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

N = SSS.N;
n_r = SSS.n_r;
n_c = SSS.n_c;

SSS_mat = zeros(n_r*N, n_c*N);
I = eye(n_c*N);

for i = 1:n_c*N
    SSS_mat(:,i) = SSSmatvec(SSS, I(:,i));
end

end

