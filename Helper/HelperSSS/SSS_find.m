function SSS = SSS_find(fullmatrix, N, n_r, n_c, n_w)
% This will take a  full matrix, and find the SSS contructors
% this function will find an SSS represesentation of the matrix fullmatrix.
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

for i=N:-1:1;
    SSS.D{i}=fullmatrix(1+ (i-1)*n_r:i*n_r,   1+ (i-1)*n_c:i*n_c);
end
[SSSupper]= SSS_findupper(fullmatrix, N, n_r, n_c, n_w);
[SSSlower]= SSS_findupper(fullmatrix', N, n_c, n_r, n_w);
SSSlower = SSS_transpose(SSSlower);

SSS.N=N;
SSS.n_r=n_r*ones(N,1);
SSS.n_c=n_c*ones(N,1);
SSS.n_w = n_w*ones(N,1);
SSS.P=SSSlower.P;
SSS.Q=SSSlower.Q;
SSS.R=SSSlower.R;

SSS.U=SSSupper.U;
SSS.W=SSSupper.W;
SSS.V=SSSupper.V;

end


