function SSS_mat = SSS_construct(SSS)
% This will take the chandraskaran contructors and matrix sizes and
% actually build an SSS matrix
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

n_r = SSS.n_r;
n_c = SSS.n_c;
if size(SSS.R,2) > 5+max(n_r,n_c)*17
    % use ivo's idea
    SSS_mat = SSS_constructvec(SSS);
else
    % use my idea
    SSS_mat = SSS_construct_new(SSS);
end
