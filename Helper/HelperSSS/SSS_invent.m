function SSS = SSS_invent(Nr,Nc,decay)
% SSS constructors:
% P,R,Q, D, U,W,V
%
% Justin Rice, 2010 
% Copyright (c) 2010, Delft Center of Systems and Control 

if Nr == Nc 
    N = Nr;
    n_r = ones(N,1);
    n_c = ones(N,1);
elseif Nr > Nc 
    N = Nc;
    n_r = ones(N,1);
    n_c = ones(N,1);
    num = Nr-Nc;
    for i = 0:floor(num/N) 
        id = randsample(N,num);
        n_r(id) = n_r(id)+1;
    end
elseif Nr < Nc
    N = Nr;
    n_r = ones(N,1);
    n_c = ones(N,1);
    num = Nc-Nr;
    for i = 0:floor(num/N) 
        id = randsample(N,num);
        n_c(id) = n_c(id)+1;
    end
end

n_r = [n_r;1];
n_c = [n_c;1];
n_w = zeros(N+1,1); 
for i = 1:N+1
   n_w(i) = 2;
end

P = cell(N,1); R = cell(N,1); Q = cell(N,1); 
D = cell(N,1);
U = cell(N,1); W = cell(N,1); V = cell(N,1);
for i = 1:N
    
   D{i,1} = 2*ones(n_r(i),n_c(i))+0.1*randn(n_r(i),n_c(i));
   
   V{i,1} = ones(n_w(i),n_c(i))+0.1*randn(n_w(i),n_c(i));
   U{i,1} = ones(n_r(i),n_w(i))+0.1*randn(n_r(i),n_w(i));
   Q{i,1} = ones(n_w(i),n_c(i))+0.1*randn(n_w(i),n_c(i));
   P{i,1} = ones(n_r(i),n_w(i))+0.1*randn(n_r(i),n_w(i));

   W{i,1} = decay*eye(n_w(i))+diag(0.01*randn(n_w(i),1)); 
   while srad(W{i,1}) > 0.99  
        W{i,1} = W{i,1}/1.01;
   end
   R{i,1}= decay*eye(n_w(i))+diag(0.01*randn(n_w(i),1));  
   while srad(R{i,1}) > 0.99  
        R{i,1} = R{i,1}/1.01;
   end
   
end

% now make SSS object:
SSS.P = P;
SSS.R = R;
SSS.Q = Q;
SSS.D = D;
SSS.U = U;
SSS.W = W;
SSS.V = V;
SSS.N = N;
SSS.n_r = n_r;
SSS.n_c = n_c;
SSS.n_w = n_w;

end