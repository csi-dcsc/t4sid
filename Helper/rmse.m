function r = rmse(y_est, y)
% Compute the root mean square error between the vectors y_est and y
%
% Mario Voorsluys, 2015 
% Copyright (c) 2018, Delft Center of Systems and Control 

r = sqrt(sum((y(:) - y_est(:)).^2) / numel(y));

end