function [X,Y] = get_ss_output(n,sys,U)
% Computes the output of a tensor state-space model using n-mode products
% 
% INPUT:
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% sys: contains the factor matrices of A,B,C
% U: cell dx1, 
% OUTPUT:
% X: cell Ntx1 (Nt is the number of temporal samples in the dataset), state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(sys.A);
Nt = length(U);
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = zeros(fliplr(n));
for k = 1:Nt
	X{k+1} = tmprod(X{k},sys.A,[d:-1:1])+tmprod(U{k},sys.B,[d:-1:1]);
	Y{k} = tmprod(X{k},sys.C,[d:-1:1]);
end

end
