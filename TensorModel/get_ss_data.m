function [U,X,Y] = get_ss_data(param,sys,Nt,snr)
% Generate input-output data with zero initial conditions
%
% INPUT:
% param: contains n, d, I, J such that
% sys: contains A, B, C such that 
% Nt: scalar, number of temporal samples in the dataset
% snr: scalar, signal to noise ratio
% OUTPUT:
% U: cell Ntx1, input
% X: cell Ntx1 state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(param.I);
U = cell(Nt,1); 
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = zeros(fliplr(param.n));
fJ = fliplr(param.J);
for k = 1:Nt
	U{k} = randn(fliplr(param.I));
	% using mode-n products
	X{k+1} = tmprod(X{k},sys.A,d:-1:1)+tmprod(U{k},sys.B,d:-1:1);
	Y{k} = tmprod(X{k},sys.C,d:-1:1);
	temp = awgn(vec(Y{k}),snr);
    Y{k} = reshape(temp,fJ);
end

end
