function X = generate_mat(row,col,opt)
% Generate matrices with additional structure
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

if exist('opt','var') && isfield(opt,'struct'), struct = opt.struct; else struct = 'SSS'; end
if exist('opt','var') && isfield(opt,'decay'), decay = opt.decay; else decay = 0.95; end

if strcmp(struct,'SSS') == 1
    temp = SSS_invent(row,col,decay);
    X = SSS_construct_new(temp);
elseif strcmp(struct,'random') == 1
    X = randn(row,col);
elseif strcmp(struct,'positive') == 1
    X = randn(row,col);
    X = abs(X);
elseif strcmp(struct,'toeplitz') == 1 || strcmp(struct,'sparse') == 1
    Bc = randn(row,1); Br = randn(col,1);
    Bc = sort(abs(Bc),'descend').*sign(Bc); Br = sort(abs(Br),'descend').*sign(Br);
    Bc(1) = max(abs(Bc(1)),abs(Br(1)))*sign(Bc(1));
    Br(1) = Bc(1);     
    if strcmp(struct,'toeplitz') == 1
        X = toeplitz(Bc,Br);
    elseif strcmp(struct,'sparse') == 1    
        X = toeplitz(Bc,Br)+0.5*randn(row,col);
        b = decay*min(row,col);
        d = row/col;    
        P = zeros(row,col);
        for i = 1:row
            for j = 1:col
                if abs(i-d*j) < b
                    P(i,j) = 1;
                end
            end
        end
        X = X.*P;
    end
end
X = X./max(X(:));

        
end