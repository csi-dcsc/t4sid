function MAT = get_full_mat(M)
% Computes the large matrix from the factor matrices. 
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

[~,r,d] = size(M);
J = zeros(1,d);
I = zeros(1,d);
for x = 1:d
    J(x) = size(M{1,1,x},1);
    I(x) = size(M{1,1,x},1);
end
MAT = cell(d,1);
for x = 1:d
    MAT{x,1} = zeros(prod(J),prod(I));
    for j = 1:r 
        MAT = MAT+kron(M(1,j,:));
    end
end

end
