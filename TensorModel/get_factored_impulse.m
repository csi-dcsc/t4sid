function M = get_factored_impulse(sys,param,s)
% Generate the sequence of factored impulse responses for the tensor
% state-space model
% 
% Baptiste Sinquin, October 2018
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(param.I);
M = cell(s,1,d);
for x = 1:d
    for i = 1:s
        M{i,1,x} = sys.C{x}*sys.A{x}^(i-1)*sys.B{x};           
    end
end

end
