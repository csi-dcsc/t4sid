function cf = cost_p(d,v,Z,X,H_op,lambda,N)
% 
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

temp = 0;
for x = 1:d
    eHi = exp(H_op(v,x));
    temp = temp+trace(-Z{x}'*eHi)+1/2*norm(X{x}-eHi,'fro').^2;
end
cf = lambda*norm(N*v,2).^2+temp;
        
end