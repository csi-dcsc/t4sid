function [,accum] = get_matrices(param,H_hat,n_init,n_end)
% Estimate the state-space matrices and evaluate the quality of the 
% estimation on validation data. 
%
% INPUT
% H_hat
% Uval
% Yval
% I: vector 1xd, dimension for input array
% J: vector 1xd, dimension for output array
% s: length of FIR filter
% n: true system order
% maxVAF: range of VAF values for different system orders. for each system
% order, the maximum is stored for all initialition parameters of the
% low-rank bilinear algorithm
% 
% OUTPUT
% n_mean: mean of the selected system order
% VAF: Variance Accounted For
% time: time for realizing the system matrices from a block-Hankel matrix
% maxVAF: range of VAF values for different system orders.
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

I = param.I;
J = param.J;
%

%
id = 1;
for nh = n_init:n_end
    tic  
    [n_temp,A_hat,B_hat,C_hat,accum] = estimate_ABC(U,S,V,sv,I,J,nh);
    time_mat = toc;
    sys.A = A_hat; sys.B = B_hat; sys.C = C_hat;
    id = id+1;
end
%

time = time_svd+time_mat;        

end