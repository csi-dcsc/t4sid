function A_HAT = get_A_stable(U,J)
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

nn = size(U,2);
A_HAT = pinv(U(1:end-J,:))*U(J+1:end,:);
% ensure Schur stability
lambda = 0;
while max(abs(eig(A_HAT)))>1
    lambda = lambda+1e-2;
    A_HAT = inv(U(1:end-J,:)'*U(1:end-J,:)+lambda*eye(nn))*U(1:end-J,:)'*U(J+1:end,:);
end

end