function [xx, residuals] = run_admm_for_BCU_update(param,x,s,mu,Q,A,b,H_op,H_opa,HH)
% Run ADMM
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: July 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% Initialize options
if exist('opt','var') && isfield(opt,'maxiter'), maxiter = opt.maxiter; else maxiter = 2e3; end
if exist('opt','var') && isfield(opt,'epsabs'),  epsabs = opt.epsabs;   else epsabs = 1e-6; end
if exist('opt','var') && isfield(opt,'epsrel'),  epsrel = opt.epsrel;   else epsrel = 1e-3; end

I = fliplr(param.I);
J = fliplr(param.J);

% Initialize variables 
s1 = (s+1)/2;
rx = s1*J(x);
cx = s1*I(x);
X = zeros(rx,cx);
Z = zeros(rx,cx);
s = size(b,1);
y2 = zeros(s,1); 
n = size(Q,1);
xx = zeros(n,1);
%
mat = 2*mu*Q'*Q+(A'*A+HH(x));
imat = inv(mat);
imat = sparse(imat);

% ADMM iterations
residuals = zeros(maxiter,2);
for iter = 1:maxiter 
    % Update x
    vec = A'*(-y2+b)+H_opa(-Z+X,x);
    xx = imat*vec;
    Hx = H_op(xx,x);

    % Update X
    Xp = X;
    [U,S,V] = svd(Hx+Z,'econ'); 
    X = U*max(0,S-1)*V';
    
    % Update duals
    y2 = y2 + (A*xx-b);
    Z = Z + (Hx-X);
    
    % Update residuals
    normrp = norm(Hx-X,'fro');
    normrd = norm(H_opa(Xp-X,x),2);
    residuals(iter,:) = [normrp normrd];
    
    % Update tolerance
    epsp = epsrel*max([norm(Hx,'fro'),norm(X,'fro')]) + sqrt(rx*cx)*epsabs;
    epsd = epsrel*norm(H_opa(Z,x)) + sqrt(n)*epsabs;
    
    % Check stopping criteria
    if normrp <= epsp && normrd <= epsd
        break;
    end
end

