function [xx,residuals] = run_admm_for_NN_update(H_op,H_adj,HH,Ho,M_op,M_adj,MM,Mo,I,J,s,d)
% Run ADMM
% TODO: add stopping criterion following the guidelines in Boyd.
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% Initialize options
if exist('opt','var') && isfield(opt,'maxiter'), maxiter = opt.maxiter; else maxiter = 1e3; end
if exist('opt','var') && isfield(opt,'epsabs'),  epsabs = opt.epsabs;   else epsabs = 1e-6; end
if exist('opt','var') && isfield(opt,'epsrel'),  epsrel = opt.epsrel;   else epsrel = 1e-3; end

% Initialize variables 
s1 = (s+1)/2;
Xh = cell(d,1);
Xhp = cell(d,1);
Zh = cell(d,1);
for x = 1:d
    Xh{x} = zeros(s1*J(x),s1*I(x));
    Zh{x} = zeros(s1*J(x),s1*I(x));
end
Xm = cell(d,1);
Xmp = cell(d,1);
Zm = cell(d,1);
for i = 1:s-1
    for j = 1:d-1
        Xm{i,j} = zeros(2);
        Zm{i,j} = zeros(2);
    end
end
tempH = cell(d,1);
tempM = cell(s-1,d-1);
%
imat = inv(diag(HH)+MM); 
imat = sparse(imat);
%
n1 = prod(J)*prod(I)*s1^2;
n2 = size(imat,1);

% ADMM iterations
residuals = zeros(maxiter,2);
iter = 1;
while iter < maxiter
    
    % Update x    
    for x = 1:d
        tempH{x} = -Zh{x}+(Xh{x}-Ho{x});  
    end
    for i = 1:s-1
        for j = 1:d-1
            tempM{i,j} = -Zm{i,j}+(Xm{i,j}-Mo{i,j});
        end
    end
    vec = H_adj(tempH)+M_adj(tempM);
    xx = imat*vec;
    Hx = H_op(xx);
    Mx = M_op(xx);

    % Update X
    id = 1;
    for x = 1:d
        temp = Xh{x};
        [U,S,V] = svd(Hx{x}+Ho{x}+Zh{x}); 
        Xh{x} = U*max(0,S-1)*V';
        Xhp{x} = Xh{x}-temp;
        id = id+1;
    end
    for i = 1:s-1
        for j = 1:d-1
            temp = Xm{i,j};
            [U,S,V] = svd(Mx{i,j}+Mo{i,j}+Zm{i,j}); 
            Xm{i,j} = U*max(0,S-1)*V';
            Xmp{i,j} = Xm{i,j}-temp;
            id = id+1;
        end
    end

    % Update duals and residuals
    normrp = 0; normrd = 0;
    for x = 1:d
        Zh{x} = Zh{x} + (Hx{x}+Ho{x}-Xh{x});
        normrp = normrp + norm(Hx{x}+Ho{x}-Xh{x},'fro');
        normrd = normrd + norm(H_adj(Xhp));
    end
    for i = 1:s-1
        for j = 1:d-1
            Zm{i,j} = Zm{i,j} + (Mx{i,j}+Mo{i,j}-Xm{i,j});
            normrp = normrp + norm(Mx{i,j}+Mo{i,j}-Xm{i,j},'fro');
            normrd = normrd + norm(M_adj(Xmp),'fro');
        end
    end
    residuals(iter,:) = [normrp normrd];
    
    % Update tolerance
    epsp = epsrel*max([sum(arrayfun(@(x) norm(x{:},'fro'), Hx)),sum(arrayfun(@(x) norm(x{:},'fro'), Ho)),...
        sum(arrayfun(@(x) norm(x{:},'fro'), Zh))]) + sqrt(n1)*epsabs;
    epsd = epsrel*norm(H_adj(Zh)) + sqrt(n2)*epsabs;
    
    % Check stopping criteria
    if normrp <= epsp && normrd <= epsd
        break;
    end
    
    %
    iter = iter+1;
end

