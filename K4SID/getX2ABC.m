function sys = getX2ABC(params,T,nn,Uid,s)
% Case d = 2 only. Estimates the factor matrices from the state-sequence.
% 
% INPUT
% nn : system order (fixed)
% Uu1, Vu1, Su1:  left, right singular vectors and singular values for the first unfolding of the three
% dimensional-tensor
% J: vector 1xd, dimension for output array
% Uid: cell Nid x 1, containing the input data used for identification
% s: length of FIR filter
%
% OUTPUT
% n_hat: vector of length d, containing the system orders
% A_hat: cell dx1, containing matrices of size n_hat(x) x n_hat(x)
% B_hat: cell dx1, containing matrices of size n_hat(x) x I(x)
% C_hat: cell dx1, containing matrices of size J(x) x n_hat(x)
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

J = params.J;
I = params.I;
Nid = length(Uid);
Mt = Nid-s+1;
s1 = (s+1)/2;
phi = s1-1;

% Compute the unfoldings and associated SVDs
unfolding = zeros(J(1)*phi,J(2)*Mt*phi);
for j = 1:phi
    mat = zeros(phi*J(1),J(2)*Mt);
    for i = 1:phi
        for k = 1:Mt
            mat((i-1)*J(1)+1:i*J(1),(k-1)*J(2)+1:k*J(2)) = T((i-1)*J(1)+1:i*J(1),(j-1)*J(2)+1:j*J(2),k);
        end
    end
    unfolding(:,(j-1)*J(2)*Mt+1:j*J(2)*Mt) = mat;
end
[U,S,V] = svd(unfolding,'econ');   
%
n1 = nn;
n2 = nn;
Uu_n = U(:,1:nn);
C1_hat = Uu_n(1:J(1),:);
A1_hat = get_A_stable(Uu_n,J(1));

% Re-shuffle the right singular vectors
Vt = S*V';
Vt_n = Vt(1:nn,:);
H = zeros(J(2)*phi,n2*Mt);
idx = 1;
for i = 1:phi
    for j = 1:Mt
        H((i-1)*J(2)+1:i*J(2),(j-1)*n2+1:j*n2) = Vt_n(:,(idx-1)*J(2)+1:idx*J(2))';
        idx = idx+1;
    end
end
[Uu2,Su2,Vu2] = svd(H,'econ');
Uu_n = Uu2(:,1:nn);
C2_hat = Uu_n(1:J(2),:);
A2_hat = get_A_stable(Uu_n,J(2));

% Estimate the state-sequence
Vt = Su2*Vu2';
Vt_n = Vt(1:nn,:);
XX_hat = zeros(nn,nn,Mt);
for k = 1:Mt
    XX_hat(:,:,k) = reshape(Vt_n(:,(k-1)*n2+1:k*n2),n1,n2)';
end

% we now have estimated A1, A2, C1, C2, X; it remains B1 and B2
% we solve a bilinear least-squares to estimate the Kronecker generators from the state-sequence
Nt = length(XX_hat);
OutputX = zeros(n2,n1*(Nt-1)); 
OutputXT = zeros(n1,n2*(Nt-1));
for k = 2:Nt
    OutputX(:,(k-2)*n1+1:(k-1)*n1) = XX_hat(:,:,k)-A1_hat*XX_hat(:,:,k-1)*A2_hat';
    OutputXT(:,(k-2)*n2+1:(k-1)*n2) = OutputX(:,(k-2)*n1+1:(k-1)*n1)';
end  
itermax = 10;
B2_hat = randn(n1,J(2));    
for iterALS1 = 1:itermax
    InputX = zeros(J(1),n1*(Nt-1));
    for k = 1:Nt-1
        InputX(:,(k-1)*n1+1:k*n1) = Uid{phi+1+k}*B2_hat';
    end
    B1_hat = OutputX*pinv(InputX);
    %
    InputX = zeros(J(2),n2*(Nt-1));
    for k = 1:Nt-1
        InputX(:,(k-1)*n2+1:k*n2) = Uid{phi+1+k}'*B1_hat';
    end
    B2_hat = OutputXT*pinv(InputX);
end
sys.A{1,1} = A1_hat; sys.A{2,1} = A2_hat; 
sys.B{1,1} = B1_hat; sys.B{2,1} = B2_hat;
sys.C{1,1} = C1_hat; sys.C{2,1} = C2_hat;
sys.D{1,1} = zeros(J(1),I(1)); sys.D{2,1} = zeros(J(2),I(2));

end