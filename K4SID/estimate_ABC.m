function [sys,time,acc] = estimate_ABC(param,H_hat,varargin)
% Estimate the system order and the system matrices
% 
% INPUT
% mat
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% s: length of FIR filter
% varargin: may contain prior knowledge of the order n_hat (contained
% within a vector of length d)
% 
% OUTPUT
% n_hat: vector of length d, containing the order of the factor matrices
% A_hat: cell dx1, containing the matrices of size n_hat(x) x n_hat(x)
% B_hat: cell dx1, containing the matrices of size n_hat(x) x I(x)
% C_hat: cell dx1, containing the matrices of size J(x) x n_hat(x)
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

I = param.I; J = param.J;
d = length(I);
if isempty(varargin), n_hat = zeros(1,d); else,  n_hat = varargin{1}*ones(1,d); end

% SVD
U = cell(d,1); S = cell(d,1); V = cell(d,1); sv = cell(d,1);
time_svd = 0;
tic
for x = 1:d
    tic
    [U{x},S{x},V{x}] = svd(H_hat{d-x+1}); 
    sv{x} = diag(S{x});
    temp = toc;
    time_svd = time_svd+temp;    
    sumT = sum(sv{x});
    for i = 1:length(sv{x})
        accum(x,i) = sum(sv{x}(1:i))/sumT;
    end 
end
acc = mean(accum);

tic
A_hat = cell(d,1); B_hat = cell(d,1); C_hat = cell(d,1); D_hat = cell(d,1);
id = 0;
for x = 1:d
    if sv{x}(1) > 0
        id = 1;
        if isempty(varargin)            
            logsv = log(sv{x});
            n_hat(x) = find(logsv>(max(logsv)+min(logsv))/2,1,'last');
        end
        C_hat{x} = U{x}(1:J(x),1:n_hat(x));
        Un = U{x}(:,1:n_hat(x));
        A_hat{x} = get_A_stable(Un,J(x));
        % discard aberrant cases
        if sum(vec(isnan(A_hat{x}))) > 0 || sum(vec(isinf(A_hat{x}))) > 0
            A_hat{x} = zeros(n_hat(x));
            id = 0;
        end
        Vn = S{x}(1:n_hat(x),1:n_hat(x))*V{x}(:,1:n_hat(x))';
        B_hat{x} = Vn(:,1:I(x));
        D_hat{x} = zeros(J(x),I(x));
    else
        id = 0;
    end    
end
time_mat = toc;
time = time_mat+time_svd;

if id == 0
    for x = 1:d
        A_hat{x} = 0;
        B_hat{x} = zeros(1,I(x));
        C_hat{x} = zeros(J(x),1);
        D_hat{x} = zeros(J(x),I(x));
    end
end
sys.A = A_hat;
sys.B = B_hat;
sys.C = C_hat;
sys.D = D_hat;

end