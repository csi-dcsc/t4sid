function [Alpha,H_hat,Time] =  solve_LOG(params,M_hat)
%
% Baptiste Sinquin, July 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

I = fliplr(params.I);
J = fliplr(params.J);
[s,~,d] = size(M_hat);
s1 = (s+1)/2;
lambda = logspace(0,4,10);

tic
% define the operators
L = cell(s,1,d);
for i = 1:s
    for x = 1:d
        L{i,1,x} = real(log(M_hat{i,1,x}));
    end
end
H_op = @(xx,x) call_logH_op(xx((x-1)*s+1:x*s),{L{:,1,x}},s1,J(x),I(x));

% define a mask for computing the derivative in the primal variable
% update
mask = cell(s,d);
for i = 1:s
    for x = 1:d
        mask{i,x} = zeros(s1*J(x));
        for ii = 1:s1
            for jj = 1:s1
                if ii+jj-1 == i
                    mask{i,x}((ii-1)*J(x)+1:ii*J(x),(jj-1)*J(x)+1:jj*J(x)) = ones(J(x));
                end
            end
        end
    end
end
%
N = kron(ones(1,d),eye(s));
gramN = sparse(N'*N);
t1 = toc;

% run ADMM
Alpha = cell(length(lambda),1);
Time = cell(length(lambda),1);
H_hat = cell(length(lambda),1);

parfor ii = 1:length(lambda)  
    tic
    [v,~] = run_admm_positive(I,J,H_op,N,gramN,mask,lambda(ii),s,d);
    alpha = zeros(s,d);
    for x = 1:d    
        alpha(:,x) = exp(v((x-1)*s+1:x*s));
    end 
    t2 = toc;
    Alpha{ii} = alpha;
    Time{ii} = t1+t2;
    H_hat{ii} = cell(d,1);
    for x = 1:d
        H_hat{ii}{x} = call_L_op(alpha(:,x),{M_hat{:,1,x}},params.J(d-x+1),params.I(d-x+1));
    end 
end

end
    

  

        
       