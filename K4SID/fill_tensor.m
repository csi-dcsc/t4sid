function T = fill_tensor(param,alpha,M_hat,Uid,Yid)
% Case d = 2 only. Form 3-dimensional tensor and computes virtual outputs
% outside the block-superdiagonal and forms one first (low-rank) unfolding.
%
% INPUT
% alpha_opt: vector of length s
% beta_opt: vector of length s
% J: vector 1xd, dimension for output array
% M_hat: cell sxrxd containing the estimated factor parameters 
% Uid: cell Nidx1, containing the input data used for identification
% Yid: cell Nidx1, containing the output data used for identification
%
% OUTPUT
% Tensor: 3-dimensional tensor with 3 low-rank unfoldings used for
% estimating the state-sequence
% 
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

alpha_opt = alpha(:,1);
beta_opt = alpha(:,2);

J = param.J;
[s,~,~] = size(M_hat);
Nid = length(Uid);
s1 = (s+1)/2;
phi = s1-1;
Mz = Nid-phi+1;
Mt = Nid-s+1;
T = zeros(J(1)*phi,J(2)*phi,Mz);
% with virtual outputs
for jj = 1:phi-1
    for k = 1:Mt
        for ll = 1:phi-jj
            temp = zeros(J);
            if s1 < ll-1
                for i = s1+1:ll-1
                    temp = temp-beta_opt(i+jj)*M_hat{i+jj,1,2}*Uid{(k+s1+ll-1)-i}*alpha_opt(i)*M_hat{i,1,1}';
                end
            elseif s1 >= ll-1
                for i = ll:s1
                    temp = temp+beta_opt(i+jj)*M_hat{i+jj,1,2}*Uid{(k+s1+ll-1)-i}*alpha_opt(i)*M_hat{i,1,1}';
                end
            end
            T((ll-1+jj)*J(1)+1:(ll+jj)*J(1),(ll-1)*J(2)+1:ll*J(2),k) = temp;
        end
    end
end
% data on the superdiagonal
for k = 1:Mt
    for j = 1:phi
        temp = Yid{s1+k+j-1};
        for i = 1:j-1
            temp = temp-M_hat{i,1,2}*Uid{s1+k+j-1-i}*M_hat{i,1,1}';
        end
        T((j-1)*J(1)+1:j*J(1),(j-1)*J(2)+1:j*J(2),k) = temp;
    end
end
% with virtual outputs
for jj = 1:phi-1
    for k = 1:Mt
        for ll = 1:phi-jj
            temp = zeros(J);
            if s1 < ll-1
                for i = s1+1:ll-1
                    temp = temp-beta_opt(i)*M_hat{i,1,2}*Uid{(k+s1+ll-1)-i}*alpha_opt(i+jj)*M_hat{i+jj,1,1}';
                end
            elseif s1 >= ll-1
                for i = ll:s1
                    temp = temp+beta_opt(i)*M_hat{i,1,2}*Uid{(k+s1+ll-1)-i}*alpha_opt(i+jj)*M_hat{i+jj,1,1}';
                end
            end            
            T((ll-1)*J(1)+1:ll*J(1),(ll-1+jj)*J(2)+1:(ll+jj)*J(2),k) = temp;
        end
    end
end

end