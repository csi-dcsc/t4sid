function H = call_L_op( xx, M_hat, J, I )
% Form block-Hankel matrix H with blocks of size J x I from a sequence of
% matrices M{i} multiplied with a scalar xx(i)
% Differs from H_op as the antidiagonal block-entries of H are not
% associated with an ambiguity parameter equal to 1. 
% 
% INPUTS:
% xx: vector 2*(d-1)*(s-1) containing the variables
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% OUTPUT: 
% H: cell dx1 containing a block-Hankel pattern where each factor matrix is
% multiplied with a scalar correcting for the corresponding ambiguity factor
% 
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

s = length(M_hat);
s1 = (s+1)/2;
x = zeros(J,s*I);
for i = 1:s
    x(:,(i-1)*I+1:i*I,:) = xx(i)*M_hat{i};
end
H = hankel_blk(x, s1, s1, J, I);

end
