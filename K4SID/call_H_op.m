function H = call_H_op( xx, M_hat, J, I)
% Linear operator H_op
% Differs from L_op as the antidiagonal block-entries of H are
% associated with an ambiguity parameter equal to 1. 
%
% INPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% OUTPUT: 
% H: cell dx1 containing a block-Hankel pattern where each factor matrix is
% multiplied with a scalar correcting for the corresponding ambiguity factor
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

[s,~,d] = size(M_hat);
s1 = (s+1)/2;
%
H = cell(d,1);
for x = 1:d
    v = xx((x-1)*(s-1)+1:x*(s-1));
    temp = zeros(J(x),s*I(x));
    idx = 1;
    for i = 1:s
        if i ~= s1
           temp(:,(i-1)*I(x)+1:i*I(x)) = v(idx)*M_hat{i,1,x};
           idx = idx+1;
        end
    end
    H{x} = hankel_blk(temp, s1, s1, J(x), I(x));
end

end
