function M = compute_GramM(s,d,X1,X2,mu_0)
%
% INPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
% s: length of FIR filter
% d: tensor order
% X1: matrix s-1 x d-1, used in the optimization
% X2: matrix s-1 x d-1, used in the optimization
% OUTPUT: 
% M: cell s-1 x d-1
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 
nx = (s-1)*(2*d-2);
n = (s-1)*(d-1)*2;
A = sparse(n^2,nx);
id = sparse(eye(nx));
B = zeros(n);
for ii = 1:nx   
    temp = call_M_op(id(:,ii),s,d,X1,X2,mu_0); 
    idx = 1;
    for j = 1:s-1
        for x = 1:d-1    
            B((idx-1)*2+1:idx*2,(idx-1)*2+1:idx*2)= temp{j,x};
            idx = idx+1;
        end
    end
    A(:,ii) = B(:);
end
M = A'*A;

end
