function xx = call_M_op_adj(Z,s,d,X1,X2,mu_0)
% Adjoint of linear operator M_op
%
% INPUT: 
% Z: cell s-1 x d-1
% s: length of FIR filter
% d: tensor order
% X1: matrix s-1 x d-1, used in the optimization
% X2: matrix s-1 x d-1, used in the optimization
% OUTPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% extract the small matrices
t = zeros(s-1,d);
alpha = zeros(s-1,d-2);
for j = 1:s-1
    if d == 2
        x = 1;
        t(j,1) = Z{j,x}(1,2)+Z{j,x}(1,1)*X2(j,x);
        t(j,2) = Z{j,x}(2,1)+Z{j,x}(1,1)*X1(j,x);
    else        
        x = 1;
        alpha(j,x) = Z{j,x}(1,2)+Z{j,x}(1,1)*X2(j,x)+Z{j,x+1}(1,1);
        t(j,x) = Z{j,x}(2,1)+Z{j,x}(1,1)*X1(j,x); 
        for x = 2:d-2
           alpha(j,x) = Z{j,x}(1,2)+Z{j,x}(1,1)*X2(j,x)+Z{j,x+1}(1,1);
           t(j,x) = Z{j,x}(2,1)+Z{j,x}(1,1)*X1(j,x); 
        end
        x = d-1;
        t(j,x) = Z{j,x}(2,1)+Z{j,x}(1,1)*X1(j,x); 
        t(j,x+1) = Z{j,x}(1,2)+Z{j,x}(1,1)*X2(j,x); 
    end
end
%
nx = (s-1)*(2*d-2);
xx = zeros(nx,1);
for x = 1:d
    xx((x-1)*(s-1)+1:x*(s-1)) = mu_0*t(:,x);
    if x < d-1
        xx(numel(t)+(x-1)*(s-1)+1:numel(t)+x*(s-1)) = mu_0*alpha(:,x); 
    end
end

end
 


