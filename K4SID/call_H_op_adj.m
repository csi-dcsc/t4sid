function xx = call_H_op_adj(Z,M_hat,J,I)
% Adjoint of linear operator H_op
%
% INPUT: 
% Z: cell dx1 containing block-Hankel matrices of size s1*J(x) x s1*I(x)
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% OUTPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

[s,~,d] = size(M_hat);
s1 = (s+1)/2;
nx = (s-1)*(2*d-2);
xx = zeros(nx,1);
%
v = [1:s1-1]';
scale = [v;flipud(v)];

for x = 1:d
    % extract blocks from Hankel matrix
    ZZ = Z{x}(:,1:I(x));
    for i = 1:s1-1
       ZZ = [ZZ;Z{x}(J(x)*(s1-1)+1:J(x)*s1,i*I(x)+1:(i+1)*I(x))]; 
    end
    %
    temp = zeros(s1,1);
    idx = 1;
    for i = 1:s
        if i ~= s1
            temp(idx) = reshape(M_hat{i,1,x},1,J(x)*I(x))*reshape(ZZ((i-1)*J(x)+1:i*J(x),:),J(x)*I(x),1);
            idx = idx+1;
        end
    end
    % 
    xx((x-1)*(s-1)+1:x*(s-1)) = temp.*scale;
end

end
