function [v,residuals] = run_admm_positive(I,J,H_op,N,gramN,mask,lambda,s,d)
%
% Baptiste Sinquin, July 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

itermaxADMM = 200; 
epsp = 1e-4;
itermaxGD = 50;
%
s1 = (s+1)/2;
Z = cell(d,1);
X = cell(d,1);
for x = 1:d
   Z{x} = zeros(s1*J(x));
   X{x} = zeros(s1*J(x));
end
residuals = zeros(itermaxADMM,2);
normrp_old = 0;
% initial guess
v = ones(s*d,1); 

for iterADMM = 1:itermaxADMM
    
    % update the prime variables with a gradient descent
    eps = Inf;
    cf_opt = 1e3;
    iterGD = 1;
    while eps > 1e-3 && iterGD < itermaxGD 
        v_old = v;      
        cf_old = cf_opt;
        % compute the gradient
        temp = zeros(s*d,1);           
        for x = 1:d
             mat = H_op(v,x);
             for i = 1:s
                eHi = exp(mat).*mask{i,x};
                eHiHiT = eHi*eHi';
                temp((x-1)*s+i) = trace(-Z{x}'*eHi+(-X{x}'*eHi)+eHiHiT);
            end
        end
        grad = lambda*gramN*v+temp; 
        % backtracking line search
        cf = @(v) cost_p(d,v,Z,X,H_op,lambda,N);
        eta = linesearch(cf,-grad,v_old,1e-3,1/2,1e-4);
        v = v_old-eta*grad; 
        cf_opt = cf(v);
        % 
        eps = abs(cf_old-cf_opt);
        iterGD = iterGD+1;
    end
    
    % update the consensus variables with singular value soft thresholding
    for x = 1:d
        eHi = exp(H_op(v,x));
        [U,S,V] = svd(eHi-Z{x}); 
        X{x} = U*max(0,S-1)*V';
    end
    
    % update the dual variables
    for x = 1:d
        eHi = exp(H_op(v,x));
        Z{x} = Z{x} + (X{x}-eHi);
    end
    
    % compute the residual
    temp = zeros(1,d);
    for x = 1:d
        eHi = exp(H_op(v,x));
        temp(x) = norm(X{x}-eHi,'fro');
    end
    normrp = sum(temp);
    residuals(iterADMM,:) = [sum(temp) norm(N*v,2).^2];
    diff = abs(normrp - normrp_old);
    normrp_old = normrp;
    
    % Check stopping criteria
    if diff <= epsp 
        break;
    end
    
end
%figure,semilogy(residuals),drawnow


