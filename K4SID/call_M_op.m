function M = call_M_op( xx, s, d, X1, X2, mu_0 )
% Linear operator M_op
%
% INPUT: 
% xx: vector 2*(d-1)*(s-1) containing the variables
% s: length of FIR filter
% d: tensor order
% X1: matrix s-1 x d-1, used in the optimization
% X2: matrix s-1 x d-1, used in the optimization
% OUTPUT: 
% M: cell s-1 x d-1
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

% pre-condition the data
t = zeros(s-1,d);
alpha = zeros(s-1,d-2);
for x = 1:d
    t(:,x) = xx((x-1)*(s-1)+1:x*(s-1));
    if x < d-1
        alpha(:,x) = xx(numel(t)+(x-1)*(s-1)+1:numel(t)+x*(s-1)); 
    end
end

% form the matrix B
M = cell(s-1,d-1);
for j = 1:s-1
    if d == 2
        x = 1;
        M{j,x} = mu_0*[t(j,1)*X2(j,x)+t(j,2)*X1(j,x) t(j,1);t(j,2) 0];
    else
        x = 1;
        M{j,x} = mu_0*[alpha(j,x)*X2(j,x)+t(j,x)*X1(j,x) alpha(j,x);t(j,x) 0];
        for x = 2:d-2    
            M{j,x} = mu_0*[alpha(j,x-1)+alpha(j,x)*X2(j,x)+t(j,x)*X1(j,x) alpha(j,x);t(j,x) 0];
        end
        x = d-1;
        M{j,x} = mu_0*[alpha(j,x-1)+t(j,x+1)*X2(j,x)+t(j,x)*X1(j,x) t(j,x+1);t(j,x) 0];
    end
end
    
end
