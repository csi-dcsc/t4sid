function H = call_logH_op( xx, impulse, l1, Nr, Nc )
% 
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

s = length(impulse);
x = zeros(Nr,s*Nc);
for i = 1:s
    x(:,(i-1)*Nc+1:i*Nc,:) = xx(i)+impulse{i};
end
H = hankel_blk(x, l1, l1, Nr, Nc);

end
