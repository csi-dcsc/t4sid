function [Alpha,H_hat,time] = solve_CR(param,M_hat)
% 
% INPUT:
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
%
% OUTPUT: 
% ALPHA: optimized sequence of ambiguity parameters [t;q]
%
% Reference for relaxing the bilinear constraint: 
% R. Doelman and M. Verhaegen, "Sequential convex relaxation for convex optimization with bilinear matrix equalities," 2016 European Control Conference (ECC), Aalborg, 2016, pp. 1946-1951.
% doi: 10.1109/ECC.2016.7810576 
% 
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: July 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

I = fliplr(param.I);
J = fliplr(param.J);
[s,~,d] = size(M_hat);
s1 = (s+1)/2;
n_eta = 6;
eta = logspace(0,2,n_eta);  
H_hat = cell(n_eta,1);
% Prepare ADMM
% x = [x_{1,1} ... x_{1,s-1} x_{2,1} ... x_{d,s-1} alpha_{1,1} ... alpha_{d,s-1}]
H_op = @(xx) call_H_op(xx,M_hat,J,I);
H_opa = @(ZZ) call_H_op_adj(ZZ,M_hat,J,I);
HH = compute_GramH(M_hat,J,I); 
HH = diag(HH);
Ho = cell(d,1);
for x = 1:d
    v = [zeros(1,s1-1) 1 zeros(1,s1-1)];
    temp = zeros(J(x),s*I(x));
    for i = 1:s
        temp(:,(i-1)*I(x)+1:i*I(x),:) = v(i)*M_hat{i,1,x};
    end
    Ho{x} = hankel_blk(temp, s1, s1, J(x), I(x));
end 

parfor ii = 1:n_eta    
    tic
    % initial guesses 
    X1 = ones(s-1,d-1);
    X2 = ones(s-1,d-1);
    %
    mu_0 = eta(ii);
    iter = 1;
    cost = Inf;
    itermax = 1e2;
    while iter < itermax && cost > 5e-3
        % M_op
        M_op = @(xx) call_M_op(xx,s,d,X1,X2,mu_0);
        M_opa = @(ZZ) call_M_op_adj(ZZ,s,d,X1,X2,mu_0);
        MM = compute_GramM(s,d,X1,X2,mu_0);
        %
        Mo = cell(s-1,d-1);
        for j = 1:s-1
            x = 1;
            Mo{j,1} = mu_0*[1+X1(j,x)*X2(j,x) X1(j,x);X2(j,x) 1];
            for x = 2:d-1    
                Mo{j,x} = mu_0*[X1(j,x)*X2(j,x) X1(j,x);X2(j,x) 1];
            end
        end
        %
        [xx,~] = run_admm_for_NN_update(H_op,H_opa,HH,Ho,M_op,M_opa,MM,Mo,I,J,s,d);
        % update X1, X2
        t = zeros(s-1,d);
        alpha = zeros(s-1,d-2);
        for x = 1:d
            t(:,x) = xx((x-1)*(s-1)+1:x*(s-1));
            if x < d-1
                alpha(:,x) = xx(numel(t)+(x-1)*(s-1)+1:numel(t)+x*(s-1)); 
            end
        end
        for j = 1:s-1
            if d == 2
                x = 1;
                X2(j,x) = -t(j,2);
                X1(j,x) = -t(j,1); 
            else
                x = 1;
                X2(j,x) = -t(j,x);
                X1(j,x) = -alpha(j,x);		
                for x = 2:d-2  
                    X2(j,x) = -t(j,x);
                    X1(j,x) = -alpha(j,x); 
                end
                x = d-1;
                X2(j,x) = -t(j,x);
                X1(j,x) = -t(j,x+1);
            end
        end
        % update the regularization
        if mod(iter,5) == 0
            mu_0 = mu_0*10;
        end
        % evolution of the residual
        cost = norm(prod(t')-1,2);
        % 
        iter = iter+1; 
    end
    time{ii} = toc;
    %     
    Alpha{ii} = t;
    H_hat{ii} = cell(d,1);
    for x = 1:d
        temp = H_op(xx);
        H_hat{ii}{x} = temp{x}+Ho{x};
    end
end

end