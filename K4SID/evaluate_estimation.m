function [rangeVAF,accum] = evaluate_estimation(sys,Uval,Yval,rangeVAF)

if VAF > VAFmax
    VAFmax = VAF;
    A_opt = A_hat;
    B_opt = B_hat;
    C_opt = C_hat;
end
store_vaf(id) = VAF;

if isempty(maxVAF) == 1
    maxVAF = store_vaf;
else
    maxVAF = max(store_vaf,maxVAF);
end                    

end