function [ALPHA,H_HAT,TIME] = solve_BCU(param,M_hat,opt)
% Estimate an ambiguity sequence of scalars such that H_op(x)+Ho{x} is low
% rank and allows to realize the factored state-space matrices 
%
% INPUT
% M_hat: cell sxrxd containing the estimated factor parameters 
% J: vector 1xd, dimension for output array
% I: vector 1xd, dimension for input array
% opt:
% opt.random: 0 if initial guesses set to 1, 1 for random init
%
% OUTPUT
% ALPHA: set of optimized variables, one for each regularization parameter
% TIME: optimization time, one for each regularization parameter
%
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'maxiter'), maxiter = opt.maxiter; else maxiter = 20; end
if exist('opt','var') && isfield(opt,'sub_iterations'), sub_iterations = opt.sub_iterations; else sub_iterations = 4; end
if exist('opt','var') && isfield(opt,'tol'), tol = opt.tol; else tol = 5e-3; end
if exist('opt','var') && isfield(opt,'muRange'), muRange = opt.muRange; else muRange = logspace(1,3,5); end

[s,~,d] = size(M_hat);
I = fliplr(param.I);
J = fliplr(param.J);

% quadratic norm on slack variable via \| Qx \|_2
Q = zeros(s,2*s);    
Q(:,s+1:2*s) = eye(s);
% relaxation of bilinear equality into: Ax = b
b = ones(s,1);

H_HAT = cell(length(muRange),1);
TIME = cell(length(muRange),1);
parfor ii = 1:length(muRange)
    tic
    mu = muRange(ii);
    alpha = ones(s,d);
    % Related to low-rank reduced-size systems
    % i.e with the operator H s.t H(x) = X, block Hankel
    H_op = @(xx,x) call_L_op(xx,{M_hat{:,1,x}},J(x),I(x));
    H_opa = @(ZZ,x) call_L_op_adj(ZZ,{M_hat{:,1,x}},J(x),I(x));
    HH = @(x) compute_GramL({M_hat{:,1,x}},J(x),I(x));
    %
    iter = 1;
    cost = Inf;
    while iter < maxiter && cost > tol
        for x = 1:d		    
            AA = [diag(prod(alpha(:,[1:x-1 x+1:d]),2)) -eye(s)];
            [xx,~] = run_admm_for_BCU_update(param,x,s,mu,Q,AA,b,H_op,H_opa,HH);
            alpha(:,x) = xx(1:s);
        end
        if norm(alpha,'fro') < 1e-6
            break;
        end           
        if mod(iter,sub_iterations) == 0
           mu = mu*5;
        end
        cost = norm(prod(alpha,2)-1,2)/norm(ones(s,1),'fro');
        iter = iter+1; 
    end
    TIME{ii} = toc;
    ALPHA{ii} = alpha;
    H_HAT{ii} = cell(d,1);
    for x = 1:d
        H_HAT{ii}{x} = call_L_op(alpha(:,x),{M_hat{:,1,x}},J(x),I(x));
    end   
end
    
end



