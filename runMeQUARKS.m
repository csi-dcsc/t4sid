% QUARKS: Identification of multi-dimensionals autoregressive models with
% sums-of-Kronecker parametrization
% 
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

% generate the model 
params = [];
params.I = [10 11];            % dimension of input grid
params.J = [15 10];            % dimension of output grid
d = length(params.I);          % tensor order
params.r = 2;                  % Kronecker rank
s = 5;                        % length of FIR filter
opt.struct = 'random';
M = SOK_fir(params,s,opt); 

% generate input-output data
Nt = 1e3;                     % number of temporal samples
Nid = floor(2/3*Nt);          % number of points in the identiifcation batch
Nval = Nt-Nid;                % number of points in the validation batch
snr = 20;                     % signal to noise ratio
[Uid,Yid] = SOK_iofir(params,Nid,snr,M);
[Uval,Yval] = SOK_iofir(params,Nval,snr,M);

% estimate a quarks model
opt = [];
opt.lambda = 0;               % regularization parameter
tic
[M_hat,norm_residual] = QUARKS(params,Uid,Yid,s,opt);   
time = toc;
figure,semilogy(norm_residual),title('Residual least squares for ALS'),grid on,drawnow

% check on validation data
Yhat_val = SOK_ofir(params,M_hat,Uval);
VAF = get_vaf(Yval,Yhat_val);
fprintf('QUARKS. VAF: %6.2f Time(s): %6.2f \n',VAF,time);



