% K4SID: Kronecker Structured large-Scale SubSpace IDentification
% 
% Identification of multi-dimensional deterministic state-space models 
% with Kronecker parametrization. The stochastic case is handled using 
% replacing the input with the output. It is assumed I_i = J_i, for all 
% i = 1..d. In this code, differents methods are proposed for estimating
% - recursive or batch-wise identification of the factored Markov
% parameters using the QUARKS
% - estimation of an ambiguity parameter solving a multilinear low-rank
% optimization via either Block-Coordinate Update (BCU) with slack variables, 
% or a convex relaxation using a nuclear norm regularization. Both 
% methods require a regularization parameter whose initial value matters: 
% it may be fixed (or given in a specified range) for a given size. Quicker
% algorithms require to have some knowledge of the hyperparameters used 
% especially in the second step. For now, we carry out these computations 
% in parallel.
% - The state-space matrices are then estimated from a SVD on the block-Hankel 
% matrices.
% 
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Edit: April 2018
%
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

params.r = 1;                  % Kronecker rank, fixed to 1
params.I = [10 10];             % dimensions of the input data (total : prod(I))
params.J = params.I;            % dimensions of the output data (total : prod(J))
d = length(params.I);          % tensor order 
params.n = 10*ones(1,d);        % order of factor matrix A_i   
         
%% generate the model and the input-output data
opt.struct = 'random';%'positive';%
sys = SOK_ss(params,opt); 
s = 15;                       % odd, length of FIR filter
s1 = (s+1)/2;
M = get_factored_impulse(sys,params,s);

% generate input-output data on Nid temporal samples
Nt = floor(10*d*s*max(params.J)*3/2);
Nid = floor(2/3*Nt);
Nval = Nt-Nid;
snr = Inf;
[Uid,Xid,Yid] = SOK_ioss(params,sys,Nid,snr);
[Uval,~,Yval] = SOK_ioss(params,sys,Nval,snr);

%% estimate a quarks model
plot_cv = 0;                 
batch = 1;                     
rls = 0;    
if batch == 1 
    optALS = [];
    optALS.lambda = 0;          
    if strcmp(opt.struct,'positive') == 1, batch = 1; optALS.struct = 'positive'; end
    tic;
    [M_hat,norm_residual] = QUARKS(params,Uid,Yid,s,optALS);   
    data.quarks(1) = toc;
    if plot_cv == 1
        figure,semilogy(norm_residual),grid on,
        xlabel('Number of ALS iterations'),ylabel('Residual least squares for ALS'),drawnow
    end
    data.quarks(2) = norm_residual(find(norm_residual>0,1,'last'));
elseif rls == 1   
    forgetting_factor = 1;
    tic
    [M_hat,iter,cv] = QUARKS_RLS(params,Uid,Yid,s,forgetting_factor);
    data.quarks(1) = toc;
    if plot_cv == 1
        figure,semilogy(iter(1:Nid-1,2),'Linewidth',0.5)               
        ylabel('Relative RMSE for the output'),xlabel('Time sample'),grid on, drawnow
        figure,semilogy(cv(1:Nid-1,1:d)),
        ylabel('Frobenius norm between two consecutive updates'),xlabel('Time sample'),grid on,drawnow
        figure,semilogy(cv(1:Nid-1,d+1)),
        xlabel('Time sample'),grid on,drawnow
    end
    data.quarks(2) = iter(end,2);
end
t = zeros(s,d); 
for i = 1:s
    for x = 1:d                    
        t(i,x) = pinv(vec(M_hat{i,1,x}))*vec(M{i,1,x});  
    end
end
v = prod(t,2);             
data.quarks(3) = norm(v-1)/norm(ones(s,1),2);  

% VAF on validation data
Yhat_val = SOK_ofir(params,M_hat,Uval);
VAF = get_vaf(Yval,Yhat_val);
data.quarks(4) = VAF;
data.quarks(5) = s*sum(params.J.^2); 
fprintf('VAF (with QUARKS): %6.2f\n',data.quarks(4));

%% estimate a sequence of ambiguity factors t_ij
% choose a method for minimizing the low-rank cost function subject to
% bilinear constraints: Block-Coordinate Update with slack variables or
% sequential relaxation using the nuclear norm
method = 'bcu';  
if strcmp(opt.struct,'positive') == 1, method = 'log'; end

if strcmp(method,'bcu') == 1 
    [alpha,H_hat,time] = solve_BCU(params,M_hat); 
elseif strcmp(method,'cr') == 1
    [alpha,H_hat,time] = solve_CR(params,M_hat);
elseif strcmp(method,'log') == 1
    [alpha,H_hat,time] = solve_LOG(params,M_hat);
end
   
n_init = params.n(1)-5;
n_end = params.n(1)+5;
N = length(H_hat);
VAFmax = cell(N,1); n_opt = cell(N,1); 
sys_hat = cell(N,1); time_k4sid = cell(N,1);
for ii = 1:N
    VAFmax{ii} = -1; n_opt{ii} = []; sys_hat{ii} = [];
    for n = n_init:n_end
        [sys_temp,time_mat,~] = estimate_ABC(params,H_hat{ii},n);                    
        X0 = zeros(size(sys_temp.A{1},1));
        [~,Yhat_val] = SOK_oss(sys_temp,Uval,X0);
        VAF = get_vaf(Yval,Yhat_val);   
        if VAF > VAFmax{ii}
           VAFmax{ii} = VAF;
           n_opt{ii} = n;
           sys_hat{ii} = sys_temp;
           time_k4sid{ii} = time_mat+time{ii};
        end
    end
end
% find the best 
VAF = -1;
for ii = 1:length(H_hat) 
    if VAFmax{ii} > VAF
        VAF = VAFmax{ii};
        ii_opt = ii;
    end
end            
data.k4sid(1) = time_k4sid{ii_opt};
data.k4sid(2) = VAF;
fprintf('VAF (with K4SID): %6.2f\n',data.k4sid(2));

if d == 2
    % estimate the state-sequence beforehand
    tic
    T = fill_tensor(params,alpha{ii_opt},M_hat,Uid,Yid); 
    time_t = toc;
    VAFmax = -1;
    for n = n_init:n_end
        tic
        sys = getX2ABC(params,T,n,Uid,s);     
        time = toc;
        X0 = zeros(n);
        [~,Yhat_val] = SOK_oss(sys,Uval,X0);
        VAF = get_vaf(Yval,Yhat_val);    
        if VAF > VAFmax
           VAFmax = VAF;
           n_opt = n;
           sys_hat = sys;
           time_opt = time;
        end
    end    
end
data.k4sid(3) = time_opt;
data.k4sid(4) = VAFmax;
fprintf('VAF (with K4SID-2): %6.2f\n',data.k4sid(4));

