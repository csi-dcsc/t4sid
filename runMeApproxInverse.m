% Test if matrix that is a sum of a few kronecker products 
% has a similar low-rank kronecker inverse or not
% Also as a function of the decay in singular values corresponding to the Kronecker
% rearrangement representation
% 
% Peter Varnai, July 2017 
% Edited by Baptiste Sinquin, April 2018
% Copyright (c) 2018, Delft Center of Systems and Control 

clear all; close all; clc;

% Experiment description
M = 2;                                  % number or Kronecker sums
decay = [0.5 1 1.5 2 3 4 5 6.5 8 10];   % the larger, the steeper the decay of the singular values for the reshuffled matrix
useSym = 0;                             % Make matrices symmetric if useSquare
useSSS = 1;
n = 20;                                 % Size of matrices if useSquare
ml = n; nl = n;
mr = n; nr = n;
N = 2;                                  % Kronecker rank for inverse

% Generate random matrix as a sum of N Kronecker products
B = cell(1,M);
C = cell(1,M);
for i = 1:M
    B{i} = randn(ml,nl);
    C{i} = randn(mr,nr); 
    if (useSSS)
        temp = SSS_invent(n,n,0.95);
        B{i} = SSS_construct_new(temp);
        %figure,imagesc(log10(abs(B{i}))),axis square
        temp = SSS_invent(n,n,0.95);
        C{i} = SSS_construct_new(temp);
    end
    if (useSym)
       B{i} = (B{i} + B{i}')/2; 
       C{i} = (C{i} + C{i}')/2; 
    end    
end

% Approximation measures
X0 = cell(N,1); Y0 = cell(N,1);
rng(12);   % allows testing different initial conditions for same problem
for i = 1:N
    X0{i} = randn(n);
    Y0{i} = randn(n);
end
F = {speye(n), speye(n)};
kronOpts.relSol = 1e-3;
kronOpts.relObj = [];
kronOpts.maxIter = 100;
kronOpts.useFast = true;
kronOpts.fastRestart = 0;
kronOpts.normalize = false;
kronOpts.showInfo = false;
kronOpts.symmetric = false;

% Plot singular values of matrix and its inverse for different decays
figTensorA = figure;
figTensorInvA = figure;
figSingInvA = figure;
figSingA = figure;

num = length(decay);
style = '-x';
colors = distinguishable_colors(num); 
legendStr = cell(1,num);
nucTensorA = zeros(num,1);
nucTensorInvA = zeros(num,1);
invIters = zeros(num,1);
residual = zeros(num,1);
for k = 1:num
    legendStr{k} = sprintf('$\\delta = %1.1f$', decay(k));

    % Assemble A
    A = zeros(ml*mr, nl*nr);
    for i = 1:M
        A = A + exp(-(i-1)*decay(k))*kron(B{i},C{i});
    end

    % Construct inverse
    [U,S,V] = svd(A);
    invA = V*pinv(S)*U';

    % Check number of iterations needed to approximate inverse with Kronecker rank M;
    Bi = cell(1,M);
    Ci = cell(1,M);
    for i = 1:M
        Bi{i} = exp(-(i-1)*decay(k))/2*B{i};
        Ci{i} = exp(-(i-1)*decay(k))/2*C{i};
    end
    [~,~,iterNum,~,res] = denseKroneckerALS(F, Bi, Ci, X0, Y0, N, kronOpts);	
    residual(k) = res(iterNum);
    invIters(k) = iterNum;		

    % Calculate singular values of rearrangements of both A and its inverse
    tensorA = SOK_reshuffle(A,ml,nl,mr,nr);
    singTensorA = svd(tensorA); 
    tensorinvA = SOK_reshuffle(invA,ml,nl,mr,nr);
    singTensorInvA = svd(tensorinvA); 

    % Normalize singular values
    singTensorA = singTensorA/singTensorA(1);
    singTensorInvA = singTensorInvA/singTensorInvA(1);

    % Calculate relative nuclear norm of first term
    nucTensorA(k) = 1/(sum(singTensorA) / sum(singTensorA(1:1)));
    nucTensorInvA(k) = 1/(sum(singTensorInvA) / sum(singTensorInvA(1:1)));

    % Calculate singular values of original matriecs
    singInvA = (1./diag(S)); 
    singA = (diag(S)); 

    % Normalize singular values
    singInvA = singInvA/singInvA(end); singInvA(end:-1:1) = singInvA;
    singA = singA/singA(1);

    % Plot results
    figure(figTensorA); semilogy(singTensorA(1:M),style,'Linewidth',.5,'color',colors(k,:)); hold on;
    xlabel('Index'),ylabel('Singular values for reshuffled matrix')
    figure(figTensorInvA); semilogy(singTensorInvA,style,'Linewidth',.5,'color',colors(k,:)); hold on;
    xlabel('Index'),ylabel('Singular values for reshuffled matrix of the true inverse')
    figure(figSingA); semilogy(singA,style,'Linewidth',.5,'color',colors(k,:)); hold on;
    xlabel('Index'),ylabel('Relative nuclear norm')
    figure(figSingInvA); semilogy(singInvA,style,'Linewidth',.5,'color',colors(k,:)); hold on;
    xlabel('Index'),ylabel('Relative nuclear norm')
end
figure,semilogy(decay,residual),grid on,
xlabel('Steepness factor for decay in singular values of original matrix'),
ylabel('Residual error')