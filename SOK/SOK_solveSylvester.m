function [Z,D,Y,time,kpA,res] = SOK_solveSylvester(matA,matQ)

[d,r] = size(matQ);
n = size(matA{1,1},1);
A = cell(d,1); C = cell(d,1);
E = cell(d,1); B = cell(d,1);
for x = 1:d   
    A{x,1} = matA{1}; 
    C{x,1} = matA{2}';
    E{x,1} = eye(n);
    B{x,1} = eye(n);
    
end

F = zeros(n^2,r); G = zeros(n^2,r);
for j = 1:r
    F(:,j) = -vec(matQ{1,j});
    G(:,j) = vec(matQ{2,j});
end

% number of ADI shifts
kpA = 5;
res = Inf;
Z = 0; D = 0; Y = 0;
while res > 1e-3 && kpA < 15
    
    kmA = kpA; l0A = kpA+kmA;
    kpB = kpA; kmB = kpB; l0B = kmB+kpB;

    % generate shifts (here: approx. small+large eigenvals of both pencils)
    tic
    v = eig(matA{1});
    eigA = kron(v,v);
    eig_descend = sort(eigA,'descend');
    rwp = eig_descend(1:kpA);
    eig_ascend = sort(eigA,'ascend');
    rwm = eig_ascend(1:kmA);
    sA = sort([rwp;rwm]);
    sA = sA(1:l0A);
    
    v = eig(matA{2}');
    eigB = 1./kron(v,v);
    eig_descend = sort(eigB,'descend');
    rwp = eig_descend(1:kpB);
    eig_ascend = sort(eigB,'ascend');
    rwm = eig_ascend(1:kmB);
    sB = sort([rwp;rwm]);
    sB = sB(1:l0B);
    
    [sA,sB] = ordershifts(sA,sB);
    threshold = -15;
    if min(log10(nonzeros(abs(imag([sA;sB]))))) < threshold
        res = -1; time = 0; 
    else
        % run LR-ADI
        maxit = 150;
        tol = 1e-6;
        try
           [Z,D,Y,res] = lr_gfadi_rk(A,B,E,C,F,G,sA,sB,maxit,tol);
        catch
            res = Inf; 
        end
        time = toc;
    end
    kpA = kpA+1;
end
kpA = kpA-1;


end