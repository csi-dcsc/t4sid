function K = SOK_dRicNewtonHewer(A,B,Q,R,ra)

[d,~] = size(A);
n = zeros(d,1);
for x = 1:d
    n(x) = size(A{x},1);
end
At = SOK_transpose(A);
Bt = SOK_transpose(B);
% initialize
X = cell(d,1);
lambda = 1e-6;
mu = 1;
ll = mu^(1/d);
for x = 1:d
    X{x} = ll*eye(n(x));
end
%X_old = X;
%
itermax = 20;
%residual = zeros(itermax,1);
iter = 1;
while iter < itermax 
    % update Riccati
    [~,Kk] = update_Riccati(A,B,Q,R,X,ra);
    b = SOK_isnull(Kk);
    if b == 0        
        temp = SOK_multiply(Kk,Bt);
        Ak = SOK_substract(At,temp);
        Ak = SOK_truncate(Ak,ra,lambda);
    else
        Ak = At;
    end
    % C_op = @(K) Q+K*R*K';
    Kt = SOK_transpose(Kk);
    T = SOK_multiplyXYZ(Kk,R,Kt);
    C = SOK_add(Q,T); 
    Ck = SOK_truncate(C,ra,lambda);
    if SOK_srad(Ak) < 1
        X = SOK_dlyap(Ak,Ck,ra,'Smith');
    else 
        break
    end
    %
    %residual(iter,1) = norm(get_mat(X)-get_mat(X_old),'fro');
    %X_old = X;
    % 
    iter = iter+1;
end
K = Kk;
%figure,semilogy(residual)

end