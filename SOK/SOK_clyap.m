function X = SOK_clyap(Z,r)
% Compute the solution of the continuous Lyapunov equation. Z is a 2x2
% cell which contains the factor matrices corresponding to the matrix
% [A' zeros(N);-Q -A]
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

M = Z{1,1};
[d,~] = size(M);
c = 1/(2^(1/d));
% 
Id = cell(d,1);
for x = 1:d
    Id{x} = speye(size(Z{1,1}{1},1)); 
end
%
itermax = 30; 
iter = 1;
%
lambda = 1e-9;
r_trunc = r;
r_inv = r_trunc;
%
Z_approx = cell(2);
iZ = cell(2);
%
while iter < itermax
    % truncate and approximate inverse
    for i = 1:2
        Z_approx{i,i} = SOK_truncate(Z{i,i},r_trunc,lambda);
        [iZ{i,i},~] = SOK_invert(Z_approx{i,i},r_inv);
    end
    Z_approx{2,1} = SOK_truncate(Z{2,1},r_trunc,lambda);
    B = SOK_multiply(iZ{2,2},Z_approx{2,1});
    temp = SOK_multiply(B,iZ{1,1}); 
    iZ{2,1} = SOK_truncate(temp,r_inv,lambda);
    for j = 1:r_inv
       iZ{2,1}{1,j} = -iZ{2,1}{1,j}; 
    end 
    %
    for ii = 1:2 
        for jj = 1:2
            if ii >= jj
                for x = 1:d
                    id = 1;
                    for j = 1:r_trunc
                       Z{ii,jj}{x,id} = c*Z_approx{ii,jj}{x,j};
                       id = id+1;
                    end
                    for j = 1:r_inv
                       Z{ii,jj}{x,id} = c*iZ{ii,jj}{x,j}; 
                       id = id+1;
                    end    
                end
            end
        end
    end 
    %
    iter = iter+1;
end
X = Z;

end
