function [X,Y] = SOK_oss(sys,U,X0)
% Computes the output of a tensor state-space model using n-mode products
% 
% INPUT:
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% sys: contains the factor matrices of A,B,C
% U: cell dx1, 
% OUTPUT:
% X: cell Ntx1 (Nt is the number of temporal samples in the dataset), state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = size(sys.A,1);
n = zeros(1,d);
for x = 1:d
    n(x) = size(sys.A{x},1);
end
Nt = length(U);
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = X0;
for k = 1:Nt
	X{k+1} = tmprod(X{k},sys.A,1:d)+tmprod(U{k},sys.B,1:d);
	Y{k} = tmprod(X{k},sys.C,1:d)+tmprod(U{k},sys.D,1:d);
end

end

