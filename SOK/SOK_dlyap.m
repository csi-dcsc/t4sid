function [P,time] = SOK_dlyap(A,Q,r_approx,method)
% Solve the discrete Lyapunov equation when the matrices A and Q are a sum
% of Kronecker factors. The integer r_approx is the Kroneker rank of the
% solution.
% Method: Smith, ALS or ADI. 
% Restriction to d = 2 except for Smith. 
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,~] = size(A);
n = zeros(1,d);
for x = 1:d
    n(x) = size(A{x,1},1);
end
%
if strcmp(method,'Smith') == 1

    tic
    lambda = 1e-8;
    mu = 1e-8;
    %
    Y = Q;
    U = A;
    V = SOK_transpose(U);

    itermax = 40;
    iter = 1;
    epsilon = Inf;
    residual = zeros(itermax,1);
    while iter < itermax && epsilon > 1e-6
        % approximate Y with K-rank r
        [~,r] = size(Y);
        if r_approx < r
            Y = SOK_truncate(Y,r_approx,lambda);  
        end  
        % compute U*Y*V 
        T1 = SOK_multiply(U,Y);
        T1 = SOK_truncate(T1,r_approx,lambda);
        T2 = SOK_multiply(T1,V); 
        T2 = SOK_truncate(T2,r_approx,lambda);
        Y = SOK_add(Y,T2);
        % compute U^2 and V^2 (and truncate!)
        U = SOK_multiply(U,U);
        U = SOK_truncate(U,r_approx,mu);
        V = SOK_transpose(U);
        % evaluate stopping criterion
        epsilon = 0;
        for x = 1:d
            for j = 1:size(U,2)
                epsilon = epsilon + norm(U{x,j},'fro')/numel(U{x,j});
            end
        end  
        if epsilon > 1e4
            break
        end
        residual(iter) = epsilon;
        %
        iter = iter+1;
    end
    %figure,semilogy(residual),drawnow
    P = SOK_truncate(Y,r_approx,lambda);  
    time = toc;

elseif strcmp(method,'ALS') == 1
    
    n = n(1);
    tic
    r = size(Q,2);
    q1 = zeros(n^2,r);
    q2 = zeros(n^2,r);
    for j = 1:r
        q1(:,j) = vec(Q{1,j});
        q2(:,j) = vec(Q{2,j});
    end
    % initial guess
    xx = cell(2,1);
    for i = 1:2
        xx{i} = rand(n^2,r_approx);
    end
    iter = 1;
    itermax = 50;
    %residual = zeros(itermax,1);
    grad_epsilon = Inf;
    epsilon_old = Inf;
    while iter < itermax && grad_epsilon > 1e-3
        % update x1
        for i = 1:d
            if i == 1
                i1 = 2; i2 = 1;
            elseif i == 2
                i1 = 1; i2 = 2;
            end
            % alpha = xx{i1}'*kron(A{i1}'*A{i1},A{i1}'*A{i1})*xx{i1};
            mati1 = A{i1}'*A{i1};
            temp = zeros(n^2,r_approx);
            for j = 1:r_approx
                temp(:,j) = vec(mati1*reshape(xx{i1}(:,j),n,n)*mati1');
            end
            alpha = xx{i1}'*temp;
            temp = xx{i1}'*xx{i1};
            %Mat = kron(alpha',kron(A{i2}'*A{i2},A{i2}'*A{i2}))'+kron(temp',eye(n^2))...
            %    -2*kron((xx{i1}'*kron(A{i1}',A{i1}')*xx{i1})',kron(A{i2},A{i2}));
            M = cell(d,3);
            mati2 = A{i2}'*A{i2};
            M{1,1} = kron(alpha,mati2');
            M{2,1} = mati2';
            M{1,2} = kron(temp',eye(n));
            M{2,2} = eye(n);
            T = zeros(n^2,r_approx);
            for j = 1:r_approx
                T(:,j) = vec(A{i1}*reshape( xx{i1}(:,j), n, n)*A{i1}');
            end
            temp = xx{i1}'*T;            
            M{1,3} = kron(-2*temp,A{i2});
            M{2,3} = A{i2};
            % N_hat = 2*kron(A{i2},A{i2})'*q1*q2'*kron(A{i1},A{i1})*xx{i1};
            for j = 1:r
                tempL(:,j) = 2*vec(A{i2}'*reshape(q1(:,j),n,n)*A{i2});
                tempR(:,j) = vec(A{i1}'*reshape(q2(:,j),n,n)*A{i1})'*xx{i1};                
            end
            N = tempL*tempR';
            % O = -2*q1*q2'*xx{i1};
            temp = q2'*xx{i1};
            O = -2*q1*temp;  
            Y = N+O;
            % SOK_solve with conjugate gradient
            Mt = SOK_transpose(M);
            MM = SOK_add(M,Mt);
            %MM = SOK_truncate(MM,ra^2,1e-9);  
            [xx{i2},~] = SOK_conjgrad(MM,-vec(Y));    
            xx{i2} = reshape(xx{i2},n^2,r_approx);
        end
        % evaluate residual 
        P = cell(d,r_approx);
        for j = 1:r_approx
            P{1,j} = reshape(xx{1}(:,j),n,n);
            P{2,j} = reshape(xx{2}(:,j),n,n);
        end
        AP = SOK_multiply(A,P);
        At = SOK_transpose(A);
        APAt = SOK_multiply(AP,At);
        mP = SOK_times(P,-1);
        APAtmP = SOK_add(APAt,mP);
        epsilon = sqrt(SOK_evalcost(APAtmP,SOK_times(Q,-1)));
        %epsilon = norm(kron(A{1},A{1})*xx{1}*xx{2}'*kron(A{2},A{2})'-xx{1}*xx{2}'+q1*q2','fro')
        grad_epsilon = abs(epsilon-epsilon_old);
        epsilon_old = epsilon;
        iter = iter+1;
        %residual(iter) = epsilon;
    end
    time = toc;
    %figure,semilogy(residual),grid on

elseif strcmp(method,'ADI') == 1
    % Not enabled yet.
    % method 3: with factored ADI method for A*X*C - E*X*B = F*G' 
    % [Z,D,Y,time] = SOK_solveSylvester(A,Q);
    % P = Z*D*Y';
    
end

 

end


