function y = SOK_MVM(A,b)
% Works for d = 2 only

[d,r] = size(A);
n = zeros(1,d);
for i = 1:d
    n(i) = size(A{i,1},1);
end

bmat = reshape(b,n(2),n(1));
Y = zeros(fliplr(n));
for j = 1:r
    Y = Y+tmprod(bmat,{A{:,j}},d:-1:1); 
end
y = vec(Y);
    
end