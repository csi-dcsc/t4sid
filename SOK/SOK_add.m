function T = SOK_add(X,Y)
% Add X and Y from their Kronecker factors
%
% Baptiste Sinquin, July 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 
 
[d,rx] = size(X);
[~,ry] = size(Y);
r = ry+rx;
T = cell(d,r);
for x = 1:d
    id = 1;
    for j = 1:rx
        T{x,id} = X{x,j};
        id = id+1;
    end
    for j = 1:ry
        T{x,id} = Y{x,j};
        id = id+1;
    end
end

end