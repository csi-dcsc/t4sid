function M = SOK_hada(X,Y)
% Compute Hadamard product between two matrices written as sums of
% Kronecker products
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(X);
M = cell(d,1);
for x = 1:d
   M{x} = X{x}.*Y{x}; 
end


end