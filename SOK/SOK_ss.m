function sys = SOK_ss(param,opt)
% Generate tensor state-space model
%
% INPUT:
% the struct param contains: 
% d: scalar, tensor order
% n: row vector 1xd (d is the tensor order), order for each factor matrix in A
% I: row vector of dimension equal to the tensor order, contains the dimensions of the input grid
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
%
% the struct opt contains: 
% an indication on the structure
% th: scalar, threshold >1 to ensure the spectral radius of each factor matrix in A is Schur stable
% OUTPUT:
% sys
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

if exist('opt','var') && isfield(opt,'threshold'), threshold = opt.threshold; else threshold = 1.05; end
if exist('opt','var') && isfield(opt,'ub'), ub = opt.ub; else ub = 1e4; end

n = param.n;
r = param.r;
I = param.I;
J = param.J;
d = length(I);

% Generate system
A = cell(d,r); 
B = cell(d,r); 
C = cell(d,r);
D = cell(d,r);
for i = 1:d
    for j = 1:r
        % state transition matrix
        A{i,j} = generate_mat(n(i),n(i),opt);
        while max(abs(eig(A{i,j}))) > 0.97
            A{i,j} = A{i,j}/threshold;
        end
        % input- and output- matrices
        B{i,j} = generate_mat(n(i),I(i),opt);
        C{i,j} = generate_mat(J(i),n(i),opt);
        D{i,j} = zeros(J(i),I(i)); 
    end
end
%
if prod(n) < ub
    Ag = SOK_getmat(A);
    val = srad(Ag);
    while val > 0.99  
        x = randperm(d,1);
        j = randperm(r,1);
        A{x,j} = A{x,j}/threshold;
        val = srad(SOK_getmat(A));
    end
end
sys.A = A;
sys.B = B;
sys.C = C;
sys.D = D;


end
