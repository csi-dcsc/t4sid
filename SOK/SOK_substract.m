function T = SOK_substract(X,Y)
% Compute X-Y from their factor matrices
%
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 
 
[d,rx] = size(X);
[~,ry] = size(Y);
r = ry+rx;
T = cell(d,r);
for x = 1:d
    id = 1;
    for j = 1:rx
        T{x,id} = X{x,j};
        id = id+1;
    end
    for j = 1:ry
        if x == 1
            T{x,id} = -Y{x,j};
        else
            T{x,id} = Y{x,j};
        end
        id = id+1;
    end
end

end