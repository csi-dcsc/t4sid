function T = SOK_multiply(X,Y)
% Matrix-matrix multiplication using the Kronecker factors only.
% 
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 
 
[d,rx] = size(X);
[~,ry] = size(Y);
r = ry*rx;
T = cell(d,r);
for x = 1:d
    id = 1;
    for jy = 1:ry
        for jx = 1:rx
            T{x,id} = X{x,jx}*Y{x,jy};
            id = id+1;
        end
    end
end

end