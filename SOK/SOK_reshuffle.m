function RA = SOK_reshuffle(A, m, n, p, q)
% SOK_reshuffle - applies the rearrangement operator to the given
% matrix, assuming a kronecker product of size m x n and p x q
% If A = B kron C, then RA = vec(B)vec(C)'

if (issparse(A))
    [r,c,v] = find(A);
    c0 = floor((c-1)/q) + 1;
    r0 = floor((r-1)/p) + 1;
    nr = m*(c0-1) + r0;
    nc = p*(c-q*(c0-1)-1) + r - p*(r0-1);
    RA = sparse(nr,nc,v,m*n,p*q);
else
    RA = zeros(m*n,p*q);
    for i = 1:m
        for j = 1:n
            RA((j-1)*m+i,:) = vec(A((i-1)*p+1:i*p,(j-1)*q+1:j*q))';
        end
    end
end


end

