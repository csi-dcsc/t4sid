function [x, k] =  SOK_conjgrad( A, b)
%
% x0: initial point
% A: Matrix A of the system Ax=b
% C: Preconditioning Matrix can be left or right
% mit: Maximum number of iterations
% stol: residue norm tolerance
% x: Estimated solution point
% k: Number of iterations done 

mit = 100;
tol = 1e-15;
%
[d,~] = size(A);
n = zeros(1,d);
for i = 1:d
    n(i) = size(A{i,1},1);
end
x0 = randn(prod(n),1);
x = x0;
hp = 0;
rp = 0;
u = 0;
k = 0;

ra = b-SOK_MVM(A,x0);
while norm(ra, inf) > tol && k < mit
    %ha = C \ ra;
    ha = ra;
    k = k + 1;
    hpp = hp;
    rpp = rp;
    hp = ha;
    rp = ra;
    t = ( (rp)') * hp;
    if (k == 1)
        u = hp;
    else
        u = hp + (t / (( (rpp)') * hpp)) * u;
    end
    Au = SOK_MVM(A,u);
    a = t / (( (u)') * Au);
    x = x + a * u;
    ra = rp - a * Au;
    %residual(k) = norm(ra, 2);
end
%figure,semilogy(residual),grid on,

end
        
