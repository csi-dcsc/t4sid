function [X,residual] = SOK_invert(Y,rX)
% Compute an approximate inverse for a matrix written with a sum of few
% Kronecker products
%
% Peter Varnai, Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,rY] = size(Y);
J = zeros(1,d);
for x = 1:d
    J(x) = size(Y{x,1},1);
end
modes = cell(d,1);
Id = cell(d,1);
X = cell(d,rX);
for x = 1:d
    modes{x} = [1:x-1 x+1:d];
    Id{x} = speye(J(x)); 
    % choose initial guesses
    for j = 1:rX
        X{x,j} = rand(J(x));
    end
end
RI = cell(d,1);
for x = 1:d
    RI{x} = sparse(vec(Id{x})*vec(kron(Id(modes{x})))');   
end

% options for solving triangular equations fast
linopts.UT = true;
% precalculate some matrices and their needed factorizations
QYMAT = cell(d,1);
RYMAT = cell(d,1);
for x = 1:d
    YMAT = zeros(J(x),J(x)*rY);
    for j = 1:rY
        YMAT(:,(j-1)*J(x)+1:j*J(x)) = Y{x,j};
    end
    [QYMAT{x},RYMAT{x}] = qr(YMAT,0);
end
% ALS iterations
iter = 0;
itermax = 30;
epsilon_old = Inf;
grad_epsilon = Inf;
residual = zeros(itermax,1);
%
while iter < itermax && grad_epsilon > 1e-2
    iter = iter + 1;

    for x = 1:d        
        % Form C
        YX = SOK_multiply(Y,X);
        C = zeros(rX*rY,prod(J(modes{x}))^2);
        for j = 1:rX*rY
            C(j,:) = vec(kron(YX(modes{x},j))); 
        end 
        [QC,LC] = qr(C',0); 
        LC = LC';
        % Form H
        H = computeH(rY,rX,J(x),RYMAT{x},LC);
        [QH,RH] = qr(H,0);
        % Form X
        fk = cell(J(x),1);
        for i = 1:J(x)
            fk{i} = vec(QYMAT{x}'*(RI{x}((i-1)*J(x)+1:i*J(x),:)*QC));
            fk{i} = QH'*fk{i};            
            xx = linsolve(RH,fk{i},linopts);   
            for k = 1:rX
                X{x,k}(:,i) = xx((k-1)*J(x)+1:k*J(x));            
            end
        end
    end
    % stopping criterion
    epsilon = SOK_evalcost(Id,YX);
    grad_epsilon = abs(epsilon-epsilon_old);
    epsilon_old = epsilon;
    % evaluate solution    
    residual(iter) = sqrt(epsilon/prod(J));
end
%figure,semilogy(residual),grid on,drawnow

end
