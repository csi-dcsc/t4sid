function T = SOK_transpose(A)
% Transpose a matrix from its Kronecker factors.
% 
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,r] = size(A);
T = cell(d,r);
for x = 1:d
    for j = 1:r
        T{x,j} = A{x,j}';
    end
end

end