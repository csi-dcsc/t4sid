function cost = SOK_evalcost(Y,X)
% Compute \| Y - X \|_F^2 when both Y and X are a sum of Kronecker products
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,rX] = size(X);
[~,rY] = size(Y);
% part 1
cost = 0;
for i = 1:rY
    for j = 1:rY
        x = 1;
        a = trace(Y{x,i}'*Y{x,j});
        for x = 2:d
            a = a*trace(Y{x,i}'*Y{x,j});
        end
        cost = cost+a;
    end
end
% part 2: cross products
for i = 1:rY
    for j = 1:rX
        x = 1;
        a = trace(Y{x,i}'*X{x,j});
        for x = 2:d
            a = a*trace(Y{x,i}'*X{x,j});
        end
        cost = cost-2*a;
    end
end
% part 3
for i = 1:rX
    for j = 1:rX
        x = 1;
        a = trace(X{x,i}'*X{x,j});
        for x = 2:d
            a = a*trace(X{x,i}'*X{x,j});
        end
        cost = cost+a;
    end
end
cost = max(0,real(cost));
        
end