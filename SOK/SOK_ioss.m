function [U,X,Y] = get_ioss(param,sys,Nt,snr)
% Generate input-output data with zero initial conditions
%
% INPUT:
% param: contains n, d, I, J such that
% sys: contains A, B, C such that 
% Nt: scalar, number of temporal samples in the dataset
% snr: scalar, signal to noise ratio
% OUTPUT:
% U: cell Ntx1, input
% X: cell Ntx1 state
% Y: cell Ntx1, output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = length(param.I);
U = cell(Nt,1); 
X = cell(Nt,1); 
Y = cell(Nt,1); 
X{1} = zeros(param.n);
for k = 1:Nt
	U{k} = randn(param.I);
	% using mode-n products
    X{k+1} = tmprod(X{k},sys.A,1:d)+tmprod(U{k},sys.B,1:d);
	Y{k} = tmprod(X{k},sys.C,1:d)+tmprod(U{k},sys.D,1:d);
	temp = awgn(vec(Y{k}),snr);
    Y{k} = reshape(temp,param.J);
end

end
