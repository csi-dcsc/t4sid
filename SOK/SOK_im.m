function SOK_im(X)
% Plots the full matrix from its factors
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

figure,imagesc(abs(SOK_getmat(X))),colorbar,colormap bone,drawnow

end