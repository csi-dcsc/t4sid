function M = SOK_FIR(param,s,opt)
% Generate factor coefficient-matrices for a FIR model representation
% 
% INPUT:
% param: containing the SOK parameters
% s: temporal order
% opt contains additional information about the structure for the factor
% matrices
% OUTPUT:
% M: cell of size s x r x d, which contains the factor matrices
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

r = param.r;
I = param.I;
J = param.J;
d = length(I);

M = cell(s,r,d);
for x = 1:d
    for j = 1:r
        for i = 1:s
            M{i,j,x} = generate_mat(J(d-x+1),I(d-x+1),opt);
        end
    end
end

end
