function  T = SOK_multiplyXYZ(X,Y,Z)
% Compute XYZ using the sums-of-Kronecker structure
% 
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,r1] = size(X);
[~,r2] = size(Y);
[~,r3] = size(Z);
T = cell(d,r1*r2*r3);
for x = 1:d
    id = 1;
    for j1 = 1:r1
        for j2 = 1:r2
            for j3 = 1:r3
                T{x,id} = X{x,j1}*Y{x,j2}*Z{x,j3};
                id = id+1;
            end
        end
    end
end

end