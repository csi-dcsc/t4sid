function a = SOK_srad(A)
% Computes the spectral radius for the matrix A whose Kronecker rank is one
% 
% INPUT:
% A: cell dx1 (d is the tensor order),
% OUTPUT:
% a: spectral radius for the matrix kron(A1,...,Ad)
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

d = size(A);
a = max(abs(eig(A{1})));
for x = 2:d
    a = a*srad(A{x});
end

end
