function Q = SOK_spdmat(params,sigma)
% Generate the Kronecker factors of a symmetric positive definite matrix
% 
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

r = params.r;
n = params.n;
d = length(params.J);

Q = cell(d,r); 
for x = 1:d
    for j = 1:r
        temp = generate_mat(n(x),n(x));
        Q{x,j} = temp+temp';
        Q{x,j} = sigma*Q{x,j}/max(Q{x,j}(:));
    end
end

end
