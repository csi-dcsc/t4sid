function [X,residual] = SOK_solve(G,E,rX)
% Solve GX = E for G and E sums-of-Kronecker
% The matrix X is parametrized with a sum of rX Kronecker products
%
% Peter Varnai, Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,rG] = size(G);
[~,rE] = size(E);
J_G = zeros(1,d);
C_E = zeros(1,d);
for x = 1:d
    J_G(x) = size(G{x,1},1);
    C_E(x) = size(E{x,1},2);
end
modes = cell(d,1);
X = cell(d,rX);
for x = 1:d
    modes{x} = [1:x-1 x+1:d];
    % choose initial guesses
    for j = 1:rX
        X{x,j} = rand(J_G(x),C_E(x));
    end
end
% re-organize the factors
f = cell(size(E));
for x = 1:d
    for j = 1:rE    
       f{x,j} = vec(E{x,j});
    end
end
% options for solving triangular equations fast
linopts.UT = true;
% precalculate some matrices and their needed factorizations
QYMAT = cell(d,1);
RYMAT = cell(d,1);
for x = 1:d
    YMAT = zeros(J_G(x),J_G(x)*rG);
    for j = 1:rG
        YMAT(:,(j-1)*J_G(x)+1:j*J_G(x)) = G{x,j};
    end
    [QYMAT{x},RYMAT{x}] = qr(YMAT,0);
end
% ALS iterations
iter = 0;
itermax = 30;
epsilon_old = Inf;
grad_epsilon = Inf;
residual = zeros(itermax,1);
while iter < itermax && grad_epsilon > 1e-6
    iter = iter + 1;

    for x = 1:d        
        % Form C
        YX = SOK_multiply(G,X);
        C = zeros(rX*rG,J_G(modes{x})*C_E(modes{x}));
        for j = 1:rX*rG
            C(j,:) = vec(kron(YX(modes{x},j))); 
        end 
        [QC,LC] = qr(C',0); 
        LC = LC';
        % Form H
        H = computeH(rG,rX,J_G(x),RYMAT{x},LC);
        [QH,RH] = qr(H,0);
        % Form X
        fk = cell(C_E(x),1);
        for i = 1:C_E(x)
            fk{i} = 0;
            for j = 1:rE                
                fk{i} = fk{i} + vec((QYMAT{x}'*f{x,j}((i-1)*J_G(x)+1:i*J_G(x),:))*(f{modes{x},j}'*QC));
            end
            fk{i} = QH'*fk{i};            
            xx = linsolve(RH,fk{i},linopts);   
            for k = 1:rX
                X{x,k}(:,i) = xx((k-1)*J_G(x)+1:k*J_G(x));            
            end
        end
    end
    % stopping criterion
    epsilon = SOK_evalcost(E,YX);
    grad_epsilon = abs(epsilon-epsilon_old);
    epsilon_old = epsilon;
    % evaluate solution    
    residual(iter) = epsilon;
end


end
