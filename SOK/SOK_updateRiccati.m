function [X,K] = SOK_updateRiccati(A,B,Q,R,X,ra)
% Compute an update of the Riccati equation with a sums-of-Kronecker
% parametrization of the matrices. 
% R_op = @(X) Q+A'*X*A-X-(A'*X*B)*inv(R+B'*X*B)*(A'*X*B)';
% K_op = @(X) (A'*X*B)*inv(R+B'*X*B);
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

% default value for regularizing the truncation step
lambda = 1e-9;

%% first compute Kalman gain
% solve GK = E
At = SOK_transpose(A);
E = SOK_multiplyXYZ(At,X,B);
E = SOK_truncate(E,ra,lambda);
Et = SOK_transpose(E);
Bt = SOK_transpose(B);
F = SOK_multiplyXYZ(Bt,X,B);
F = SOK_truncate(F,ra,lambda);
G = SOK_add(R,F);
G = SOK_truncate(G,ra,lambda);
Gt = SOK_transpose(G);
Kt = SOK_solve(Gt,Et,ra);
K = SOK_transpose(Kt);

%% second, update the Riccati equation
E = SOK_multiplyXYZ(At,X,A);
F = SOK_substract(Q,X);  
mat1 = SOK_add(E,F);
mat1 = SOK_truncate(mat1,ra,lambda);
mat2 = SOK_multiply(K,Et);
mat2 = SOK_truncate(mat2,ra,lambda);
X = SOK_substract(mat1,mat2);
X = SOK_truncate(X,ra,lambda);

end


