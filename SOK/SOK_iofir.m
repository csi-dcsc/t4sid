function [U,Y] = SOK_iofir(param,Nt,snr,M)
% Generate input-output data with FIR model structure
% 
% INPUT: 
% param: structure containing the SOK parameters
% Nt: scalar, number of temporal samples
% snr: scalar, signal-to-noise ratio
% M: cell of size s x r x d, contains the factor matrices
%
% OUTPUT:
% U: cell of size Nt x 1, containing the input data
% Y: cell of size Nt x 1, containing the noisy output data
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

[s,~,~] = size(M);
r = param.r;
I = param.I;
J = param.J;
d = length(I);

% Generate input-output data
U = cell(Nt,1); 
Y = cell(Nt,1); 
for k = 1:Nt
	U{k} = randn(I);
    Y{k} = zeros(J);
end
for k = s+1:Nt
	% using mode-n products
    for i = 1:s
        for j = 1:r
            Y{k} = Y{k}+tmprod(U{k-i},{M{i,j,:}},d:-1:1);
        end
    end
	temp = awgn(vec(Y{k}),snr);
    Y{k} = reshape(temp,J);
end

end
