function Y = SOK_ofir(params,M,U)
% Computes the output of a tensor-based FIR model (using n-mode products)
% 
% INPUT:
% J: row vector of dimension equal to the tensor order, contains the dimensions of the output grid
% M: cell sxrxd: estimated factor matrices 
% U: cell Ntx1 (with Nt number of temporal samples)
% OUTPUT:
% Y: cell Ntx1, estimated output
%
% Baptiste Sinquin, April 2018 
% Copyright (c) 2018, Delft Center of Systems and Control 

[s,~,~] = size(M);
J = params.J;
r = params.r;
d = length(J);
Nt = length(U);

% Generate input-output data
Y = cell(Nt,1);
for k = 1:s
    Y{k} = zeros(J);
end
for k = s+1:Nt
    Y{k} = zeros(J);
    for i = 1:s
        for j = 1:r
            Y{k} = Y{k}+tmprod(U{k-i},{M{i,j,:}},d:-1:1);
        end
    end
end

end
