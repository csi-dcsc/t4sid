function [K,residual] = SOK_dRicNewton(A,B,Q,R,ra)

% default value for regularizing the truncation step
lambda = 1e-3;
[d,~] = size(A);
At = SOK_transpose(A);
Bt = SOK_transpose(B);
% initialize
X = cell(d,1);
n = zeros(1,d);
for x = 1:d
    n(x) = size(A{x,1},1);
    X{x} = 1e-3*eye(n(x));
end
% ALS iterations
itermax = 20;
iter = 1;
epsilon = Inf;
residual = zeros(itermax,1);
while iter < itermax && epsilon > 1e-3
    [Rk,Kk] = SOK_updateRiccati(A,B,Q,R,X,ra);

    b = SOK_isnull(Kk);
    if b == 0
        temp = SOK_multiply(Kk,Bt);
        Ak = SOK_substract(At,temp);
        Ak = SOK_truncate(Ak,ra,lambda);
    else
        Ak = At;
    end
    
    Nk = SOK_dlyap(Ak,Rk,ra,'Smith');
    X = SOK_add(X,Nk);
    X = SOK_truncate(X,ra,lambda);
    
    epsilon = 0;
    for x = 1:d
        for j = 1:size(Nk,2)
            epsilon = epsilon + norm(Nk{x,j},'fro')/numel(Nk{x,j});
        end
    end
    %
    iter = iter+1;
end
K = Kk;

end

