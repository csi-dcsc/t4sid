function T = SOK_times(X,q)
% Multiply a sums-of-Kronecker matrix with a scalar q
%
% Baptiste Sinquin, July 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 
 
[d,rx] = size(X);
r = rx;
T = cell(d,r);
for x = 1:d
    for j = 1:rx
        if x == 1
            T{x,j} = q*X{x,j};
        else
            T{x,j} = X{x,j};
        end
    end
end

end