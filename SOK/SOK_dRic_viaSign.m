function K = SOK_dRic_viaSign(A,B,R,Q,ra)

[d,~] = size(A);
Bt = SOK_transpose(B);
n = zeros(1,d);
for x = 1:d
    n(x) = size(A{x,1},1);
end

% address variables
F = A;
G1 = B;
G2 = R;
H = Q;
Id = cell(d,1);
for x = 1:d
    Id{x} = eye(n(x));
end
% G = G1*inv(G2)*G1';
iG2 = SOK_invert(G2,1);
T1 = SOK_multiply(G1,iG2);
G = SOK_multiply(T1,Bt);

% compute bilinear transform
%N = [get_mat(F) zeros(prod(n));-get_mat(H) eye(prod(n))];
%L = [eye(prod(n)) get_mat(G);zeros(prod(n)) get_mat(F)'];
%Ha = (N+L)\(N-L);
% compute N+L
NpL{1,1} = SOK_add(F,Id); 
NpL{1,2} = G; 
NpL{2,1} = SOK_times(H,-1); 
NpL{2,2} = SOK_transpose(NpL{1,1});
% invert N+L
X = SOK_invert2x2blockmat(NpL,ra);
% form N-L
Y{1,1} = SOK_substract(F,Id);
Y{1,2} = SOK_times(G,-1);
Y{2,1} = NpL{2,1};
temp = SOK_transpose(F);
Y{2,2} = SOK_substract(Id,temp);
% multiply inv(N+L) with N-L
H = SOK_multiply2x2blockmat(X,Y,ra);
Ha = [get_mat(H{1,1}) get_mat(H{1,2});get_mat(H{2,1}) get_mat(H{2,2})];
%res1 = get_rmse(mat,Ha)

% matrix sign
%figure,imagesc(abs(Ha))
%Z = compute_matrix_sign(Ha);
%figure,imagesc(abs(Z-Ha))

% start from correct H
% for i = 1:2
%     for j = 1:2
%         [temp] = cpd(reshuffle(Ha((i-1)*100+1:i*100,(j-1)*100+1:j*100),10,10),4);
%         for x = 1:2
%             for jj = 1:4
%                 H{i,j}{x,jj} = reshape(temp{1,x}(:,jj),10,10);
%             end
%         end 
%     end
% end
% Ha = [get_mat(H{1,1}) get_mat(H{1,2});get_mat(H{2,1}) get_mat(H{2,2})];
Z = compute_matrix_sign(Ha);
[W,~] = SOK_matrix_sign_for2x2Block(H,ra,Z);
%mat = [get_mat(W{1,1}) get_mat(W{1,2});get_mat(W{2,1}) get_mat(W{2,2})];
%figure,imagesc(abs(mat))
%res2 = get_rmse(mat,Z)
%pause

% solve least-squares, X_sign = pinv(left)*right
% L = [Z(1:100,101:200);Z(101:200,101:200)+eye(100)]; 
% rR = [-(Z(1:100,1:100)+eye(100));Z(101:200,1:100)];
% X_sign = pinv(L)*rR;
left{1} = W{1,2};
left{2} = SOK_add(W{2,2},Id);
right{1} = SOK_times(SOK_add(W{1,1},Id),-1);
right{2} = W{2,1};
% LL'*RR
x1 = SOK_multiply(SOK_transpose(left{1}),right{1});
x1 = SOK_truncate(x1,ra);
x2 = SOK_multiply(SOK_transpose(left{2}),right{2});
x2 = SOK_truncate(x2,ra);
x = SOK_add(x1,x2);
% LL'*LL
x1 = SOK_multiply(SOK_transpose(left{1}),left{1});
x1 = SOK_truncate(x1,ra);
x2 = SOK_multiply(SOK_transpose(left{2}),left{2});
x2 = SOK_truncate(x2,ra);
y = SOK_add(x1,x2);
% 
y = SOK_invert(y,3);
X = SOK_multiply(y,x);
X = SOK_truncate(X,ra);
% res3 = get_rmse(get_mat(X),X_sign);

% compute Kalman gain, K = inv(Bg'*X_sign*Bg+Rg)*(Bg'*X_sign*Ag);
XA = SOK_multiply(X,A);
right = SOK_multiply(Bt,XA);
XB = SOK_multiply(X,B);
BtXB = SOK_multiply(Bt,XB);
x = SOK_add(BtXB,R);
x = SOK_truncate(x,ra);
left = SOK_invert(x,ra);
K = SOK_multiply(left,right);
K = SOK_truncate(K,ra);
%
% Bg = get_mat(B);
% Ag = get_mat(A);
% Rg = get_mat(R);
% Kg = inv(Bg'*X_sign*Bg+Rg)*(Bg'*X_sign*Ag);
% res4 = get_rmse(get_mat(K),Kg);

% fprintf('Bilinear transform %6.2f, Matrix sign %6.2f, Inverse %6.2f, Kalman, %6.2f\n',res1,res2,res3,res4);

end