function F = SOK_getFactors(M,m,n,p,q,r)
% Computes the best k-Kronecker rank approximation of a matrix M.
% Implemented only for te case d = 2
% 
% INPUTS:
% M : matrix with m x n blocks which are of dimensions p x q
% k : Kronecker rank of approximation
% OUTPUTS:
% Factor matrices F
%
% Baptiste Sinquin, April 2017
% Edit: Peter Varnai, August 2017
% Copyright (c) 2018, Delft Center of Systems and Control 

TENSOR = SOK_reshuffle(M, m, n, p, q);
[U,S,V] = svd(TENSOR);
d = 2;
F = cell(d,r);
for i = 1:r
    Si_sqrt = sqrt(S(i,i));
    F{2,i} = reshape(Si_sqrt*U(:,i),m,n);
    F{1,i} = reshape(Si_sqrt*V(:,i),p,q);
end

end