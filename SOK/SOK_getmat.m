function mat = SOK_getmat(M,opt)
% Form the full matrix from its Kronecker factors
%
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

if exist('opt','var') && isfield(opt,'storage'), storage = opt.storage; else storage = 'dr'; end

if strcmp(storage,'dr') == 1
    [d,r] = size(M);
    J = zeros(1,d);
    I = zeros(1,d);
    for x = 1:d
        [J(x),I(x)] = size(M{x,1});
    end
    mat = zeros(prod(J),prod(I));
    for j = 1:r
        mat = mat+kron(M(d:-1:1,j));
    end
else    
    [~,r,d] = size(M);
    J = zeros(1,d);
    I = zeros(1,d);
    for x = 1:d
        [J(x),I(x)] = size(M{1,1,x});
    end
    mat = zeros(prod(J),prod(I));
    for j = 1:r
        M(1,j,d:-1:1)
        mat = mat+kron(M(1,j,d:-1:1));
    end
end

end