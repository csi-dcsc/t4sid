function X = SOK_truncate(F,r,lambda)
% Approximate a matrix written with a sum of Kronecker products with another matrix written with fewer products 
%
% Baptiste Sinquin, June 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,rtilde] = size(F);
if rtilde > r
    J = zeros(1,d);
    I = zeros(1,d);
    for x = 1:d
        J(x) = size(F{x,1},1);
        I(x) = size(F{x,1},2);
    end
    %
    modes = cell(d,1);
    n = zeros(d,1);
    Ybar = cell(d,1);
    Y = cell(d,1);
    X = cell(d,r);
    for x = 1:d
        modes{x} = [1:x-1 x+1:d];
        n(x) = prod(J(modes{x}))*prod(I(modes{x}));
        Ybar{x} = zeros(n(x),rtilde);
        Y{x} = zeros(J(x)*I(x),rtilde);
        for j = 1:rtilde
            Ybar{x}(:,j) = vec(kron(F(modes{x},j)));
            Y{x}(:,j) = vec(F{x,j});
        end
        % initial guess
        for j = 1:r
            X{x,j} = randn(J(x),I(x));
        end
    end
    %    
    epsilon_old = Inf;
    grad_epsilon = Inf;    
    itermax = 20;
    iter = 1;
    while iter < itermax && grad_epsilon > 1e-3
        for x = 1:d
            M = zeros(n(x),r);
            mat = zeros(r);
            for j = 1:r
                M(:,j) = vec(kron(X(modes{x},j)));
                for i = 1:r
                    mat(i,j) = sum(vec(kron(SOK_hada(X(modes{x},i),X(modes{x},j))))); 
                end
            end
            matA = Ybar{x}'*M;
            temp = Y{x}*matA;
            val = temp/(mat+lambda*eye(r)); 
            temp = reshape(val,J(x),I(x)*r);            
            for j = 1:r                
                X{x,j} = temp(:,(j-1)*I(x)+1:I(x)*j);
            end
        end
        epsilon = SOK_evalcost(X,F);
        grad_epsilon = abs(epsilon-epsilon_old);
        epsilon_old = epsilon;
        iter = iter+1;
    end
else
    X = F;
end

end

