function b = SOK_isnull(X)
% Reveals whether all factors are zero
%
% Baptiste Sinquin, October 2018 
% Copyright (c) 2018, Delft Center for Systems and Control 

[d,r] = size(X);
c = 0;
for x = 1:d
    for j = 1:r
        if nnz(X{x,j}) > 0
            c = c+1;
        end
    end
end
if c == 0
    b = 1;
else
    b = 0;
end

end