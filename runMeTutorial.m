% This tutorial explains the tensor basics and explores the properties of multi-level matrices 
% we are interested in. It is suggested to proceed with Run and Advance.
%
% Edit: July 2018
% Baptiste Sinquin, baptiste.sinquin@gmail.com
% Copyright (c) 2018, Delft Center of Systems and Control 

clc; clear;
close all;

% The identification of large-scale linear systems is relevant for many applications,
% ranging from adaptive optics on Extremely Large Telescopes to wind farm control.
% Identifying from data the spatial and temporal dynamics of a large system using 
% standard model structures and related algorithms is however challenging from the 
% computational point of view. This tutorial investigates a particular class of 
% systems that are multi-dimensional, and whose model matrices can be parametrized
% with sums of products of Kronecker.

%% We start this tutorial with a 2-level matrix. Such structure appears when 
% modeling the static input-output map for a plate deformed by actuators.
N = 32;
D = 1;
coupling = 0.9;
M = deformableMirror( N,N,D,coupling,1 );
figure,imagesc(M),axis square;
colorbar,title('2-level matrix'),drawnow

%% Apart from the fact that it is a block-matrix, it has another key structure; 
% reshuffling its entries into another matrix is of low-rank. 
display = 1;
k = N; 
[M_approx,~,~,~] = compute_Kronecker_rank(M,N,N,N,N,k,display);

%% Notice that here, the rank is one. Such particular (optimal) case appears 
% when the function is separable in both x-y dimensions. For practical 
% applications, it is very unlikely that % the so-called Kronecker rank is 
% one. Actually, when the reshuffling matrix is low-rank, the original matrix 
% can be written with a sum of few Kronecker products. 
figure,imagesc(log10(abs(M_approx-M))),axis square;
colorbar, title('log10(abs(M_{approx}-M))'),drawnow
 
%% The separability of the process that is spatially sampled is a rather strong 
% assumption. When it is only approximately the case, the Kronecker rank slightly
% increases. Let us first generate a matrix M from a non-separable
% function.
M = deformableMirror( N,N,D,coupling,2 );

%% And now, let's have a look at the rank of the reshuffled matrix.
[M_approx,~,~,accuracy] = compute_Kronecker_rank(M,N,N,N,N,k,display);
figure,semilogy(accuracy(1:N)),grid on
xlabel('Kronecker rank'),ylabel('Residual error'),drawnow

%% For a 2-level matrix with blocks of size N x N, the class of matrices we 
% are especially interested in, have a good approximation with a Kronecker 
% rank smaller than N. The lower the better for data storage. While the number
% of entries to represent the matrix is N^4 for the unstructured matrix, it
% is only 2rN^2 for a Kronecker product representation. More than only storage, 
% matrix vector multiplications are also faster. For example, for a vector x, 
% the operation kron(A,B)x is rewritten with BXA^T, where x = vec(X). We 
% refer to the papers for the theoretical evaluation of the computational 
% complexity, and compare it numerically with Matlab. Note the results are 
% likely to be different using C code. 
N = [20:10:100];
time = zeros(10,length(N),2);
for ii = 1:10
    for i = 1:length(N)
        x = randn(N(i)^2,1);
        M1 = randn(N(i));
        M2 = randn(N(i));
        % global (overparametrized) representation
        M = kron(M2',M1);
        tic
        temp = M*x;
        time(ii,i,1) = toc;
        tic
        temp = M1*reshape(x,N(i),N(i))*M2;
        time(ii,i,2) = toc;
    end
end
figure,
loglog(N,mean(time(:,:,1)),'+'),hold on
loglog(N,mean(time(:,:,2)),'x')
grid on,
ll = xlabel('Dimension of array, N');
set(ll,'interpreter','latex')
ll = ylabel('Time(s)');
set(ll,'interpreter','latex')
ll = legend('Vector form','Matrix form');
set(ll,'interpreter','latex')
set(gca,'FontSize',12.5)
drawnow

%% Such 2-level matrices can also be modeled using larger tensor orders. We 
% use again the example of 2D influence functions, and now propose to evaluate 
% the coefficients from noise-free input-output data with Alternating Least 
% Squares. We first generate some input-output data. Note the example is simple 
% as it is static map, but generalization to dynamical systems relies on
% similar ideas.
N = 32;
M = deformableMirror( N,N,D,coupling,1 );
Nt = 500;
u = randn(N^2,Nt);
y = zeros(N^2,Nt);
y(:,2:end) = M*u(:,1:end-1);
%
I = cell(4,1);
I{1} = [32 32];
I{2} = [32 8 4];
I{3} = [16 16 2 2];
I{4} = [4 4 8 8];
s = 1;
M_hat = cell(4,5);
err = zeros(4,5);
% Now evaluating the decomposition of 2-level matrices into higher-order tensor 
% decompositions. It takes some time depending on the length of the identification batch.
for i = 1:4
    for r = 1:5
        Uid = cell(Nt,1);
        Yid = cell(Nt,1);
        for k = 1:Nt
            Uid{k} = reshape(u(:,k),fliplr(I{i}));
            Yid{k} = reshape(y(:,k),fliplr(I{i}));
        end
        param.I = fliplr(I{i});
        param.J = fliplr(I{i});
        param.r = r;
        [M_hat{i,r},norm_residual] = QUARKS(param,Uid,Yid,s);
        err(i,r) = norm_residual(find(norm_residual,1,'last'));
    end
end
err

%% When increasing the tensor order from d=2, a very particular structure 
% appears on the matrices. Increasing the Kronecker rank gradually removes 
% such tendency.
opt.storage = 'rd';
figure,
temp = SOK_getmat(M_hat{1,1}(1,:,:),opt);
subplot(221),imagesc(temp),colormap bone,axis off
temp = SOK_getmat(M_hat{2,1}(1,:,:),opt);
subplot(222),imagesc(temp),colormap bone,axis off
temp = SOK_getmat(M_hat{3,1}(1,:,:),opt);
subplot(223),imagesc(temp),colormap bone,axis off
temp = SOK_getmat(M_hat{4,1}(1,:,:),opt);
subplot(224),imagesc(temp),colormap bone,axis off
drawnow
% Although the time for identifying a model here increases with increasing 
% tensor order, we have not modified the number of points in the
% identification batch although the number of parameters to be estimated
% dramatically decreased. 

%% We have used the identification algorithm as a black-box in the 
% previous points, a few operations used are worth mentioning as they are 
% standard in tensor computations. We start with the example of a 3-dimensional 
% tensor (equivalently, a tensor of order d).
I = [5 4 3];
vecA = 1:prod(I);
A = reshape(vecA,I)

%% Operations on tensors often involve to matricize them. A tensor of order
% d has d different matricizations. 
disp('1st-mode unfolding')
U1 = tens2mat(A,1)
disp('2nd-mode unfolding')
U2 = tens2mat(A,2)
disp('3st-mode unfolding')
U3 = tens2mat(A,3)

%% The operation kron(Ad,...,A1)x is rewritten using n-mode products for more 
% efficient computations. A n-mode product is a product between the n-mode
% matricization and a matrix. We first introduce a matrix:
fprintf('1st-mode product in two different lines of code:')
M = rand(2,size(U1,1));
tens2mat(tmprod(A,{M},1),1)-M*U1
disp('2nd-mode product in two different lines of code')
M = rand(2,size(U2,1));
tens2mat(tmprod(A,{M},2),2)-M*U2
disp('3st-mode product in two different lines of code')
M = rand(2,size(U3,1));
tens2mat(tmprod(A,{M},3),3)-M*U3

%% Such operations are performed for speed purposes.
% For a 2-level matrix with blocks of size N x N, the class of matrices we 
% are especially interested in, have a good approximation with a Kronecker 
% rank much smaller than N. The lower the better for data storage. While the number
% of entries to represent the matrix is N^4 for the unstructured matrix (i.e d = 1),
% it is only N^((2*(d+1))./d) for a Kronecker product representation.
d = 2:6;
N = 10:10:10^3;
ratio = zeros(length(d),length(N));
for i = 1:length(d)
    for j = 1:length(N)
        alpha = N(j)^((2*(d(i)+1))./d(i));
        ratio(i,j) = N(j)^4./alpha;
    end
end
figure,
loglog(N,ratio)
grid on,
ll=xlabel('Size of grid in 1D, N','fontsize',15);
set(ll,'interpreter','latex')
ll=ylabel('Improvement in complexity','fontsize',15);
set(ll,'interpreter','latex')
ll = legend('d=2','d=3','d=4','d=5','d=6');
set(ll,'interpreter','latex')
set(gca,'FontSize',12.5)
drawnow

%% Relationship with a Canonical Polyadic Decomposition (CPD)
% Let a matrix be written with a Kronecker product of d-terms, form the 
% reshuffled tensor equal to a product of r rank-one terms and compute a 
% CPD to recover the factor matrices (up to a scaling factor)
I = [5 6 7];
d = length(I);
r = 2;
U = cell(1,d);
M = cell(d,r);
for x = 1:d 
    U{x} = zeros(I(x)^2,r);
    for j = 1:r
        M{x,j} = randn(I(x));
        U{x}(:,j) = vec(M{x,j});
    end
end
T = cpdgen(U);
% CPD decomposition
Uhat = cpd(T,r);
That = cpdgen(Uhat);
norm(vec(That-T),2)/norm(vec(T),2)

